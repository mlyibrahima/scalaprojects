// launch spark in local mode
spark-shell \
--master "local[*]"

// launch spark in the lab
spark2-shell \
--master yarn \
--conf spark.ui.port=0
// in the jupyter
import org.apache.spark.sql.SparkSession
val spark = SparkSession.
			builder.
			config("spark.ui.port", "0").
			appName("Data Processing - Overview").
			master("yarn").
			getOrCreate

//for details config
spark.sparkContext.getConf.getAll.foreach(println)

//example how to read a delimited data from text file
//spark.read.csv
//option("inferschema", "true") permet de recuperer les types à la place de metre tout à String 
val orders = spark.
			 read.
			 option("inferschema", "true").
			 csv("/public/retail_db/orders")

//spark.read.csv
//schema permet d'inserer les entetes et leur types
val orders = spark.
			 read.
			 schema("""order_id INT, order_date TIMESTAMP,
					   order_customer_id INT, order_status STRING
			        """).
			 csv("/public/retail_db/orders")


//spark.read.csv with option
val orders = spark.
			 read.
			 schema("""order_id INT, order_date TIMESTAMP,
					   order_customer_id INT, order_status STRING
			        """).
			 option("sep", ",").
			 csv("/public/retail_db/orders")

//spark.read.format
val orders = spark.
			 read.
			 schema("""order_id INT, order_date TIMESTAMP,
					   order_customer_id INT, order_status STRING
			        """).
			 option("sep", ",").
			 format("csv").
			 load("/public/retail_db/orders")


//spark.read.json
val orders = spark.
			 read.
			 option("inferSchema", "false").
			 schema("""order_id INT, order_date TIMESTAMP,
					   order_customer_id INT, order_status STRING
			        """).
			 json("/public/retail_db_json/orders")

//spark.read.format with json
val orders = spark.
			 read.
			 option("inferSchema", "false").
			 schema("""order_id INT, order_date TIMESTAMP,
					   order_customer_id INT, order_status STRING
			        """).
			 format("json").
			 load("/public/retail_db_json/orders")

//Afficher le schema
orders.printSchema

scala> orders.printSchema
root
 |-- order_id: integer (nullable = true)
 |-- order_date: timestamp (nullable = true)
 |-- order_customer_id: integer (nullable = true)
 |-- order_status: string (nullable = true)

// to know the data types
scala> orders
res1: org.apache.spark.sql.DataFrame = [order_id: int, order_date: timestamp ... 2 more field
s]
// to priview the data
scala> orders.show
+--------+-------------------+-----------------+---------------+                             
|order_id|         order_date|order_customer_id|   order_status|
+--------+-------------------+-----------------+---------------+
|       1|2013-07-25 00:00:00|            11599|         CLOSED|
|       2|2013-07-25 00:00:00|              256|PENDING_PAYMENT|
|       3|2013-07-25 00:00:00|            12111|       COMPLETE|
|       4|2013-07-25 00:00:00|             8827|         CLOSED|
|       5|2013-07-25 00:00:00|            11318|       COMPLETE|
|       6|2013-07-25 00:00:00|             7130|       COMPLETE|
|       7|2013-07-25 00:00:00|             4530|       COMPLETE|
|       8|2013-07-25 00:00:00|             2911|     PROCESSING|
|       9|2013-07-25 00:00:00|             5657|PENDING_PAYMENT|
|      10|2013-07-25 00:00:00|             5648|PENDING_PAYMENT|
|      11|2013-07-25 00:00:00|              918| PAYMENT_REVIEW|
|      12|2013-07-25 00:00:00|             1837|         CLOSED|
|      13|2013-07-25 00:00:00|             9149|PENDING_PAYMENT|
|      14|2013-07-25 00:00:00|             9842|     PROCESSING|
|      15|2013-07-25 00:00:00|             2568|       COMPLETE|
|      16|2013-07-25 00:00:00|             7276|PENDING_PAYMENT|
|      17|2013-07-25 00:00:00|             2667|       COMPLETE|
|      18|2013-07-25 00:00:00|             1205|         CLOSED|
|      19|2013-07-25 00:00:00|             9488|PENDING_PAYMENT|
|      20|2013-07-25 00:00:00|             9198|     PROCESSING|
+--------+-------------------+-----------------+---------------+
only showing top 20 rows

//Pour afficher 10 elements et ne pas trunquer l'affichage
orders.show(10, truncate=false)
//or
orders.show(10, false)

//Describe
scala> orders.describe()
res5: org.apache.spark.sql.DataFrame = [summary: string, order_id: string ... 2 more fields]

scala> orders.describe().show(false)
+-------+------------------+-----------------+---------------+
|summary|order_id          |order_customer_id|order_status   |
+-------+------------------+-----------------+---------------+
|count  |68883             |68883            |68883          |
|mean   |34442.0           |6216.571098819738|null           |
|stddev |19884.953633337947|3586.205241263963|null           |
|min    |1                 |1                |CANCELED       |
|max    |68883             |12435            |SUSPECTED_FRAUD|
+-------+------------------+-----------------+---------------+


// Overview of Data Frame APIs
//Create Dataframe employees using Collection

val employees = List((1, "Scott", "Tiger", 1000.0, "united states"),
					 (2, "Henry", "Ford", 1250.0, "India"),
					 (3, "Nick", "Junior", 750.0, "united KINGDOM"),
					 (4, "Bill", "Gomes", 1500.0, "AUSTRALIA")
					)
// transform to Dataframe
val employeesDF = employees.
				  toDF("employee_id",
					   "first_name",
					   "last_name",
					   "salary",
					   "nationality"
					  )

//project employee first name and last name.
employeesDF.select("first_name", "last_name").show
//project all the fields except for Nationality
employeesDF.drop("nationality").show

// Overview of Functions	

// Tasks
// Let us perform a task to undestand how functions are typically used.
// Project full name by concatenating first name and last name along with other fiels excluding 
//first name and last name
//Using col and lit
import org.apache.spark.sql.functions.{col, lit, concat}
employeesDF.
	select(col("employee_id"),
		   concat(col("first_name"), lit(" "), col("last_name")).alias("full_name"),
		   col("salary"),
		   col("nationality")
		  ).
	show

//withColumn
employeesDF.
	withColumn("full_name", 
				 concat(col("first_name"), lit(" "), col("last_name"))
	
		      ).
	drop("first_name", "last_name").
	show

//Using $ and lit
import spark.implicits._
employeesDF.
	select(col("employee_id"),
		   concat($"first_name", lit(" "), $"last_name").alias("full_name"),
		   col("salary"),
		   col("nationality")
		  ).
	show

//Using SQL Style
employeesDF.
	selectExpr("employee_id",
			   "concat(first_name, ' ', last_name) AS full_name",
			   "salary",
			   "nationality").
	show


//Overview of Spark Write APIs

spark.conf.get("spark.sql.parquet.compression.codec")

// Using write.parquet without compression
orders.
	write.
	option("compression", "none").
    mode("overwrite").
	parquet("/user/itv001573/retail_db/orders")

// Using write.parquet with gzip compression
orders.
	write.
	option("compression", "gzip").
    mode("overwrite").
	parquet("/user/itv001573/retail_db/orders")

//to run hdfs command in spark-shell
//we have to sys.process._
import sys.process._
"hdfs dfs -ls /user/itv001573/retail_db/orders" !

// Using write.format("parquet")
//coalesce(1). permet d'enregistrer en un seul fichier
orders.
	coalesce(1).
	write.
	option("compression", "none").
    mode("overwrite").
    format("parquet").
	save("/user/itv001573/retail_db/orders")

// to read the file from parquet format
spark.read.parquet("/user/itv001573/retail_db/orders").printSchema 
spark.read.parquet("/user/itv001573/retail_db/orders").show