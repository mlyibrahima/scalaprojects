//For data
git clone https://github.com/dgadiraju/data.git

What is Scala?

    Scala is JVM based functional programming language.

Why Scala?

    Even though Scala is there for more than a decade (founded in 2000), it has gained a lot of momentum with Spark.
    Spark is completely developed using Scala
    On top of Spark (used for data processing), Scala also have frameworks such as Play to develop web applications

Setting up Scala on your local machine

    For Mac users – Download and install using tarball. Set PATH to the bin directory
    Windows – Download and Install using MSI
    If you have the subscription to itversity labs then Scala is pre-installed in it
    For building projects, better to install IDE’s such as IntelliJ IDE or Eclipse with Scala plugin
    For Certification point of view practice using REPL

REPL

    REPL – Read, Evaluate, Print and Loop
    Use scala command to launch REPL
    :quit to come out
    Ctrl+l to clear the screen
    We can get the help using :help command
    Use up arrow to see the history also :history to check the previous commands



As part of this topic lets get into Basic Programming Constructs in Scala.

        Declaring Variables
        Invoking Functions
        Conditional
        While loop
        For loop

Declaring Variables in Scala

We will use Scala REPL to explore variables in scala.

    Each and every variable in scala must be declared with either val or var
    val is immutable and var is mutable

val i = 2 or var i=2

Invoking Functions

A function is a group of statements that perform a task.

    prinlln is a function in scala
    To invoke a function in scala we use its object name and function name

Example

val s = "Hello World"
s.toUpperCase

For Loop

For loop is a repetitive structure which allows us to execute a block of code multiple times.
Example

for (i <- (1 to 100)) {
  println(i)
}

Task – Sum of all the given numbers

var total = 0
for (element <- (1 to 100))
  total += element

Task – Sum of even numbers from 1 to 100

var total = 0 
for (element <- (1 to 100))
  if(element % 2 ==0)
    total += element

While Loop

A while loop statement is executed repeatedly until the condition is false.

syntax

while(condition) { statement }

Example

Display Sum of even numbers and Sum of odd numbers

var lb = 1
val ub = 100
var totalEven = 0
var totalOdd = 0
while(lb <= ub) {
  if(lb % 2 == 0)
    totalEven += lb
  else
    totalOdd += lb
  lb += 1
}


In this topic, we will see what are Functions and Anonymous Functions.
Functions

Let us understand more about Functions in Scala.

    A function is a group of statements that perform a task
    We need to give function name, arguments and argument types for regular functions
    Functions are expressions (not statements)
    Functions can be returned, passed as arguments.

Syntax

def functionName(parameters : typeofparameters) : returntypeoffunction = {  
// statements to be executed  
}

Example

def sum(lb: Int, ub: Int)={
var total =0
for (element <- lb to ub) {
total += element
}
total
}

sum(1,10)

Problem Statement

Display the output for given statements

    Sum of numbers in a given range
    Sum of squares of numbers in a given range
    Sum of cubes of numbers in a given range
    Sum of multiples of 2 in a given range.

Example

def sum(func: Int => Int, lb:Int, ub:Int)= {
var total = 0
for(element <- lb to ub)
{
total += func(element)
}
total
}

def id(i: Int)= i

def sqr(i: Int)= i * i

def cube(i: Int)= i * i * i

def double(i: Int)= i * 2

sum(id, 1, 10)

sum(sqr, 1, 10)

sum(cube, 1, 10)

sum(double, 1, 10)

Anonymous Functions

    Anonymous functions need not have a name associated with it
    Anonymous functions can be assigned to variables
    Those variables can be passed as parameters
    While invoking we need to provide the functionality for the parameters which are defined as functions (for parameter i in this case)

sum(i => i, 1, 10)
sum(i => i * i, 1, 10)
sum(i => i * i * i, 1, 10)
sum(i => i * 2, 1, 10)


In this topic we will see Object Oriented Concepts – Classes
Defining a class

To define a class in Scala we declare keyword class in front of identifier. Class names should be capitalized.
syntax

class className

Example

class Order(orderId: Int, orderDate: String, orderCustomerId: Int, orderStatus: String) {
println("I am inside Order Constructor")
}

Disassemble and Decompile Scala code

    Use the javap command to disassemble a .class file to look at its signature.

Syntax

:javap -p className
//In this case its Order class

Object Creation

    To create an object in scala we use new keyword

Syntax

val or var = new className(//parameters)

Creating a function inside a class

    We can create a function inside a class
    In this case we create an toString function which will start with override keyword. Since toString overrides the pre-defined toString method, it has to be tagged with the override flag.
    By using toString function we can see the details of the object instead of byte code.

Example

class Order(orderId:Int,orderDate:String,orderCustomerId:Int,orderStatus:String)
{
println("I am inside Order Constructor")
override def toString = "Order("+ orderId+ ","+ orderDate + ","+ orderCustomerId + ","+ orderStatus +")"
}

Object Creation for Order class

var order = new Order(1, "2013-10-01 00:00:00.00",100, "COMPLETE")

Passing variables in class instead of arguments
Example

class Order(val orderId:Int,val orderDate:String,val orderCustomerId:Int,val orderStatus:String) 
{
println("I am inside Order Constructor") override def toString = "Order("+ orderId+ ","+ orderDate + ","+ orderCustomerId + ","+ orderStatus +")" 
}

Object Creation for Order class

var order = new Order(1, "2013-10-01 00:00:00.00",100, "COMPLETE")

Note

    By default if we don’t set val or var in the parameters then those are called arguments and we cannot access their elements.



var lb = 1
val ub = 100
var totalEven = 0
var totalOdd = 0
while(lb <= ub) {
  if(lb % 2 == 0)
    totalEven += lb
  else
    totalOdd += lb
  lb += 1
}

var totalEven = 0
var totalOdd = 0
for (i <- (1 to 100)) {
    if (i%2 == 0)
        totalEven+=i
    else
        totalOdd+=i
}
#Function
Syntax

def functionName(parameters : typeofparameters) : returntypeoffunction = {  
// statements to be executed  
}

Example

def sum(lb: Int, ub: Int)={
var total =0
for (element <- lb to ub) {
total += element
}
total
}

sum(1,10)
#Anonymos Function
def sum(func: Int => Int, lb:Int, ub:Int) = {
    var total = 0
    for (element <- lb to ub) {
        total+=func(element) 
    }
    total
}
def id(i:Int)=i
def sqr(i:Int)=i*i
def double(i:Int)=i*2

sum(id,1,10)
sum(i=>{
    if(i%2==0)
    i*i
    else
    i*2
   }, 1, 10)


#Class
class Order(val orderId: Int, val orderDate: String, val orderCustomerId: Int, val orderStatus: String) {
    println("I am inside Order Constructor")
    override def toString = "Order(" + orderId + "," + orderDate + "," + orderCustomerId + "," + orderStatus + ")"
}


var order = new Order(1,"2020-09-10", 2, "COMPLETE")
:javap -p Order 
# Result of :javap -p Order 
Compiled from "<console>"
public class $line12.$read$$iw$$iw$Order {
  private final int orderId;
  private final java.lang.String orderDate;
  private final int orderCustomerId;
  private final java.lang.String orderStatus;
  public int orderId();
  public java.lang.String orderDate();
  public int orderCustomerId();
  public java.lang.String orderStatus();
  public java.lang.String toString();
  public $line12.$read$$iw$$iw$Order(int, java.lang.String, int, java.lang.String);
}



Object

    Scala is more object-oriented than Java because in Scala, we cannot have static members. Instead, Scala has singleton objects
    Object is a keyword which represents singleton class.


Companion Objects

    A companion object is an object with the same name as a class and is defined in the same source file as the associated file.

Example

class Order(val orderId:Int,val orderDate:String,val orderCustomerId:Int,val orderStatus:String)
{
 println("I am inside Order Constructor")
 override def toString = "Order("+ orderId+ ","+ orderDate + ","+ orderCustomerId + ","+ 
 orderStatus +")"
}

object Order {
 def apply(orderId: Int, orderDate: String, orderCustomerId: Int, orderStatus:String): Order = {
 new Order(orderId, orderDate, orderCustomerId, orderStatus)
}
}

val order = Order.apply(1, "2013-10-01 00:00:00.000", 100,"COMPLETE")


object HelloWorld {
  def main(args: Array[String]): Unit = {
     println("Hello World")
  }
}
HelloWorld.main(Array(" "))

#Case Class
case class Order(orderId:Int, orderDate:String, orderCustomerId:Int, orderStatus:String) {
 println("I am inside Order Constructor")
 override def toString = "Order("+ orderId+ ","+ orderDate + ","+ orderCustomerId + ","+ 
 orderStatus +")"
}











As part of this topic we will see one of the important topics for building Spark applications using Scala i.e Collections.
Collections

Scala collections are categorized into 3 types

    Seq
    Set
    Map

Seq

    Sequence have length
    Elements in Sequence can be accessed using prefix
    eg: scala.Array, scala.collection.immutable.List etc
    Classes are divided into Seq classes and Buffer classes
    Vector and List are considered as Seq classes and they are more frequently used
    Array is special type of collection analogous to Java Array
    We will get into the details of Array and List
    Array is mutable while List is immutable
    Seq have 2 sub traits – IndexedSeq and LinearSeq

Set

    Set is iterable which contain unique elements (no duplicates)
    As duplicates are not allowed, there is no length (Seq have both length and index)
    Even though we can access data using index, it might not return same value always
    By default Set is immutable, but there is mutable one as well
    Let us see how we can use mutable and immutable Set in a demo
    If the Set contain elements for which implicit Ordering defined, we can use SortedSet to sort the data

Map

    Map is Iterable of Key and Value pairs
    Each element can be defined as key -> value or (key, value)

We will look into few collection classes such as Array,List,Set and Map.
Declaration of Array

val a = Array(1,2,3,4)
Declaration of List

val l = List(1,2,3,4)
Declaration of set

val s = Set(1,1,2,2,3,4)
Declaration of Map

val m = Map("Hello" -> 1 , "World" -> 2)


Now let us look into the basic map reduce operations such as Map, Filter, Reduce and Rich Collection API.
Problem Statement

Get Sum of squares of all Even Numbers in a given range

val l = (1 to 100).toList
val f = l.filter(ele => ele % 2 == 0)
val m = f.map(rec => rec * rec)
var total =0
for( e <- m) total + = e <=> m.reduce((total, ele) => total + ele)

As part of this topic, we will explore Basic I/O operations

    Read data from files
    Convert into collection
    Perform collection operation to preview the data
    Run map-reduce operations

Reading data from files

    To read files from a file system in scala there is a package called scala.io
    In scala.io we have a package named Source which is used to read files from the file system
    Important operations in Source package are as follows

Few APIs from Source to read data

    fromFile is used to read data from a file
    fromIterable is used to read data from collection
    fromURI creates Source from a file with given file: URI
    fromURL is used to read data from HTTP URL

Example 
import scala.io.Source

val orderItems = Source.fromFile("/data/retail_db/order_items/part-00000").getLines.toList
orderItems(0)
orderItems.take(10).foreach(println)
orderItems.size

val orderItemsFilter = orderItems.filter(orderItem => orderItem.split(",")(1).toInt == 2)
val orderItemsMap = orderItemsFilter.map(orderItem => orderItem.split(",")(4).toFloat)

orderItemsMap.sum

orderItemsMap.reduce((total, orderItemSubtotal) => total + orderItemSubtotal)

orderItemsMap.reduce(_ + _)


Let us see tuples in detail

    A tuple is another data structure in Scala
    It can hold heterogeneous elements
    Syntax for tuple val t = (1, "Hello World")
    Another syntax for tuple val t: (Int, String) = (1, "Hello World")
    Tuple has a handful of methods
    Elements of tuples can be accessed using _ notation (t._1 will return 1 and t._2 return Hello World)
    If there are 2 elements it is called also known as pair
    Elements of a tuple can be collections and vice versa
    Tuple with collection val t = (1, List(1, 2, 3, 4))
    Collection with tuple  val t = List((1, "Hello"), (2, "World"))
    We will use tuples quite extensively in Spark as a key value pair

Example

import scala.io.Source
val orderItems = Source.fromFile("/data/retail_db/order_items/part-00000").getLines
orderItems.take(10).foreach(println)
val t = (1,1,957,1,299.98,299.98)
t._1
print(t)

import scala.io.Source
val orderItems = Source.fromFile("/home/lyfase_service/retail_db/order_items/part-00000").getLines
val orderRevenue = orderItems.filter(_.split(",")(1).toInt==2).
    map(_.split(",")(4).toFloat).
    reduce(_ + _)


//Development Cycle - Create Program File

import scala.io.Source

object OrderRevenue {
  
    def main(args: Array[String]): Unit = {
        val orderItems = Source.fromFile("/home/lyfase_service/retail_db/order_items/part-00000").getLines
        val orderRevenue = orderItems.
            filter(_.split(",")(1).toInt==2).
            map(_.split(",")(4).toFloat).
            reduce(_ + _)
        println(orderRevenue)
    }
}

scala src/main/scala/orderRevenue.scala

// Development Cycle - Compile source code to jar using SBT

// First be to the project directory -- in ower context is 'retail'
cd code/scalaDemo/retail/

//After create a new file 'build.sbt'
vi build.sbt
//and paste this lines
name := "retail"
version := "1.0"
scalaVersion :="2.12.8"
//After run the package by this command
sbt package
//After run the program by this command <=> scala src/main/scala/orderRevenue.scala
//It's same by running this command 'scala ../retail/target/scala-2.12/retail_2.12-1.0.jar OrderRevenue'
sbt run


//ITVersity Resources
//Development Cycle – Compile changes and run jar with arguments


As part of this topic, we will explore another way of compiling and running the jar file with arguments through SBT

    We can make the changes to the code
    Recompile the jar file
    Run the jar file
    How to pass the arguments to the program

Example

import scala.io.Source
object OrderRevenue {
def main(args: Array[String]) = {
  val orderId = args(0).toInt
  val orderItems = Source.fromFile("/home/lyfase_service/retail_db/order_items/part-00000").getLines
  val orderRevenue = orderItems.filter(oi => oi.split(",")(1).toInt == orderId).
  map(oi => oi.split(",")(4).toFloat).
  reduce((t, v) => t + v)
  println(orderRevenue)
 }
}

//Run the program using this below command after rebuilding the package
sbt package
scala target/scala-2.12/retail_2.12-1.0.jar OrderRevenue 2
//or 
sbt "runMain OrderRevenue 2"