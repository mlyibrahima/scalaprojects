
Scala - Getting Started with Spark

As part of this module we will take a simple use case and try to scratch the surface of the Spark.

    Understand Data Model
    Define Problem Statement
    Creating Spark Context
    Setting Run Time Job Properties
    Reading data from CSV Files
    Apply Filtering
    Row Level Transformations
    Perform Joins
    Aggregate Data
    Perform Sorting
    Write output to Files
    Complete Script
    Validating Output

Understand Data Model

Let us understand the data model and also characteristics of the data.

    Base directory for retail_db data sets is /public/retail_db.
    It have six folders, each folder represents a separate table.
        Product Catalog Tables
            products
            categories
            departments
        Customers Table
            customers
        Transactional Tables
            orders
            order_items
    orders and order_items are related. orders is parent table and order_items is child table for orders.
    All folders have one ore more files under them.
    Each line represents a record and have values related to multiple columns. Each record is delimited or separated by comma (,).
    First field in each orders record is order_id and it is a primary key (unique and not null)
    Second field in each order_items record is order_item_order_id which is a foreign key attribute to orders order_id.
    There are other relationships as well, however they are not relevant to get started. We will primarily focus on orders and order_items data.

Define Problem Statement

Get monthly revenue considering complete or closed orders

    We will use orders and order_items data.
    orders is available at /public/retail_db/orders
    order_items is available at /public/retail_db/order_items
    We need to consider orders with COMPLETE or CLOSED status.
    Revenue can be computed using order_item_subtotal which is 5th attribute in order_items.

Creating Spark Context

Let us understand how to create Spark Context using SparkSession from org.apache.spark.sql.

    We need to have spark context to leverage both APIs as well as distributed computing framework.
    SparkSession is a wrapper class which will use existing Spark Context or create new one.
    We can customize the behavior of Spark Context created by passing properties using config or by using APIs such as appName, master etc.
    APIs are provided only for most commonly used properties.

import org.apache.spark.sql.SparkSession

val spark = SparkSession.
    builder.
    config("spark.ui.port", "0").
    appName("Getting Started - Monthly Revenue").
    master("yarn").
    getOrCreate

spark

import spark.implicits._

Setting Run Time Job Properties

Let us understand how to customize run time behavior of submitted jobs.

    Once Spark Context is created, we can customize run time behavior by using spark.conf.set.
    In our case let us set a property called as spark.sql.shuffle.partitions to 2.
    If we do not set this property, by default it will use 200 threads.

spark.conf.set("spark.sql.shuffle.partitions", "2")

Reading data from CSV Files

Let us quickly see how we can read data from CSV Files.

    Spark provide several APIs to read the files of different file formats.
    All the out of the box APIs are available under spark.read.
    In our case we have to read text files where each record is delimited or separated by comma (',').
    To create Data Frames for orders and order_items we can pass the path to spark.read.csv.
    There are other options as well which can be passed using keyword arguments. You can run help on spark.read.csv.

spark.read.csv

// Reading orders
val orders_path = "/public/retail_db/orders"
val orders = spark.
    read.
    schema("order_id INT, order_date STRING, " +
           "order_customer_id INT, order_status STRING"
          ).
    csv(orders_path)

orders.printSchema

orders.show

// Reading order_items
val order_items_path = "/public/retail_db/order_items"
val order_items = spark.
    read.
    schema("order_item_id INT, order_item_order_id INT, " +
           "order_item_product_id INT, order_item_quantity INT, " +
           "order_item_subtotal FLOAT, order_item_product_price FLOAT"
          ).
    csv(order_items_path)

order_items.printSchema()

order_items.show()

Apply Filtering

Let us see how we can filter out records in Data Frame.

    We can either use filter or where to filter the data. Both of them serve the same purpose.
    We can pass the condictions either in SQL Style or API Style.
    In this case, we have used SQL Style to check order_status for COMPLETE or CLOSED orders.
    We can perform all standard filtering conditions.

val orders_filtered = orders.
    filter("order_status in ('COMPLETE', 'CLOSED')")

orders_filtered.show

    Here is an example for API Style.

orders.
    filter(orders("order_status").isin("COMPLETE", "CLOSED")).
    show

Row Level Transformations

Let us see how we can project and also derive new fields out of existing fields leveraging functions.

    One of the ways to project data is by using select on top of Data Frame. We have to pass the column names as column type. Spark Implicits provide $ operator to pass column names as column type.
    We can also pass column names as column type using col function as well as by passing column names like this orders("order_id").
    Spark provides almost 300 pre defined functions as part of org.apache.spark..sql.functions.
    In our case we need to import and use date_format function to extract month from existing date.
    Later we will also import and use functions such as sum and round while aggregating the data.
    We can also provide meaningful names to derived fields using alias.

import org.apache.spark.sql.functions.date_format

val orders_transformed = orders_filtered.
    select($"order_id", date_format($"order_date", "yyyyMM").alias("order_month"))

orders_transformed.show

Perform Joins

Let us join both the data sets which have the fields we are interested in.

    We can join data sets using join.
    We also might have to pass join condition in case the column names are different between the data sets.
    In our case we have to join orders and order_items using orders("order_id") and order_items("order_item_order_id").

We can join original Data Frames as well and generate order_month while grouping the data as demonstrated in the Complete Script Section.

val order_details_by_month = orders_transformed.
    join(order_items, 
         orders("order_id") === order_items("order_item_order_id")
        )

order_details_by_month.show

Aggregate Data

As we have joined orders and order_items, let us perform the aggregation.

    In this case want to compute revenue for each month.
    order_month is derived field which contain both year and month.
    We can use order_month as part of groupBy so that data can be grouped. It will generate a special Data Frame of type GroupedData.

order_details_by_month.
    groupBy("order_month")

    We can now invoke aggregate functions such as sum and pass the desired field using which we want to aggregate (in this case we can pass order_item_subtotal to sum).

import org.apache.spark.sql.functions.{sum, round}

val monthly_revenue = order_details_by_month.
    groupBy("order_month").
    agg(round(sum("order_item_subtotal"), 2).alias("revenue"))

monthly_revenue.show

Perform Sorting

As we got the revenue for each month, let us sort the data so that we can review the output for the validation.

    We can use orderBy or sort to sort the data.
    By default data will be sorted in ascending order.
    In this case we are sorting the data by order_month.

val monthly_revenue_sorted = monthly_revenue.
    orderBy("order_month")

monthly_revenue_sorted.show

Write output to Files

As data is read, processed and sorted - now it is time to write data to files in underlying file system.

    In our environment /public is read only folder. You will not be able to add files under subdirectories of /public.
    Assuming you have write access to /user/[OS_USER_NAME], I have used /user/{username}/retail_db/monthly_revenue as target folder.
    ${username} is replaced by the OS user used for login using System.getProperty("user.name").
    coalesce(1) is used to write the output to one file.
    If the folder and files already exists, mode("overwrite") will replace existing folder with new files.

val username = System.getProperty("user.name")

monthly_revenue_sorted.
    coalesce(1).
    write.
    mode("overwrite").
    option("header", "true").
    csv(s"/user/${username}/retail_db/monthly_revenue")

Complete Script

Here is the complete script or program which takes care of the following:

    Create Spark Context and set the properties.
    Read the data related to different tables.
    Process the data using relevant Spark Data Frame APIs.
    Write the data back to file system

Entire Data processing and writing the data back to file system is developed using Piped approach.

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{date_format, sum, round}

val username = System.getProperty("user.name")

val spark = SparkSession.
    builder.
    config("spark.ui.port", "0").
    appName("Getting Started - Monthly Revenue").
    master("yarn").
    getOrCreate

spark.conf.set("spark.sql.shuffle.partitions", "2")
import spark.implicits._

// Reading orders
val orders_path = "/public/retail_db/orders"
val orders = spark.
    read.
    schema("order_id INT, order_date STRING, " +
           "order_customer_id INT, order_status STRING"
          ).
    csv(orders_path)

// Reading order_items
val order_items_path = "/public/retail_db/order_items"
val order_items = spark.
    read.
    schema("order_item_id INT, order_item_order_id INT, " +
           "order_item_product_id INT, order_item_quantity INT, " +
           "order_item_subtotal FLOAT, order_item_product_price FLOAT"
          ).
    csv(order_items_path)

orders.
    filter("order_status in ('COMPLETE', 'CLOSED')").
    join(order_items, orders("order_id") === order_items("order_item_order_id")).
    groupBy(date_format($"order_date", "yyyyMM").alias("order_month")).
    agg(round(sum($"order_item_subtotal"), 2).alias("revenue")).
    orderBy("order_month").
    coalesce(1).
    write.
    mode("overwrite").
    option("header", "true").
    csv(s"/user/${username}/retail_db/monthly_revenue")

username = training
spark = org.apache.spark.sql.SparkSession@410aecbf
orders_path = /public/retail_db/orders
orders = [order_id: int, order_date: string ... 2 more fields]
order_items_path = /public/retail_db/order_items
order_items = [order_item_id: int, order_item_order_id: int ... 4 more fields]

[order_item_id: int, order_item_order_id: int ... 4 more fields]

spark

Validating Output

Let us go ahead and validate the output.

    In our case we are using HDFS and hence we should be able to use HDFS commands to validate.
    Let us first list the files which will give some idea about when they are created.
    For some file formats, we will also see extension as well as compression algorithm used.

import sys.process._

val username = System.getProperty("user.name")

s"hdfs dfs -ls /user/${username}/retail_db/monthly_revenue/part*"!

-rw-r--r--   2 training training        252 2020-03-20 14:50 /user/training/retail_db/monthly_revenue/part-00000-62d47b0c-ecbc-46f6-baee-bc35761706a8-c000.csv

username = training

warning: there was one feature warning; re-run with -feature for details

0

    In case of small text files we can use cat to see the contents. It might not work if the files are compressed.
    Also, it is not a good practice to use cat for larger text files.

import sys.process._

val username = System.getProperty("user.name")

s"hdfs dfs -cat /user/${username}/retail_db/monthly_revenue/part*"!

order_month,revenue
201307,333465.46
201308,1221828.92
201309,1302255.83
201310,1171686.94
201311,1379935.36
201312,1277719.63
201401,1230221.76
201402,1217770.11
201403,1271350.99
201404,1249723.54
201405,1221679.35
201406,1179754.08
201407,955590.79

username = training

warning: there was one feature warning; re-run with -feature for details

0

