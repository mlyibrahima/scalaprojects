
Quick Recap of Scala

Let us quickly recap of some of the core programming concepts of Scala before we get into Spark.
Data Engineering Life Cycle

Let us first understand the Data Engineering Life Cycle. We typically read the data, process it by applying business rules and write the data back to different targets

    Read the data from different sources.
        Files
        Databases
        Mainframes
        APIs
    Processing the data
        Row Level Transformations
        Aggregations
        Sorting
        Ranking
        Joining multiple data sets
    Write data to different targets.
        Files
        Databases
        Mainframes
        APIs

Scala CLI or Jupyter Notebook

We can use Scala CLI or Jupyter Notebook to explore APIs.

    We can launch Scala REPL using scala command.
    We can launch the Jupyter Notebook using the jupyter notebook command.
    A web service will be started on port number 8888 by default. You are using jupyter notebook provided by us and hence you don't need to run the above command.
    We can go to the browser and connect to the web server using IP address and port number.

Here are some of the features of using Jupyter Notebook

    We should be able to explore code in interactive fashion.
    We can issue magic commands such as %%sh to run shell commands.

util.Properties.versionString

version 2.11.8

Tasks

Let us perform these tasks to just recollect how to use Scala REPL or Jupyter Notebook.

    Create variables i and j assigning 10 and 20.5 respectively.

val i = 10
val j = 20.5

i = 10
j = 20.5

20.5

    Add the values and assign it to res.

val res = i + j

res = 30.5

30.5

Basic Programming Constructs

Let us recollect some of the basic programming constructs of Scala.

    Comparison Operations (==, !=, <, >, <=, >=, etc)
        All the comparison operators return a True or False (Boolean value)
    Conditionals (if)
        We typically use comparison operators as part of conditionals.
    Loops (for)
        We can iterate through collection using for (i <- l) where l is a standard collection such as list or set.
        Scala provides special operator with to and by which will return a collection of integers between the given range.
    In Scala ; is optional before writing code in next line. However, if you want to have multiple expressions in the same line, they have to be separated by ;

Tasks

Let us perform few tasks to quickly recap basic programming constructs of Scala.

    Get all the odd numbers between 1 and 15.

(1 to 16 by 2)

Range(1, 3, 5, 7, 9, 11, 13, 15)

    Print all those numbers which are divisible by 3 from the above list.

for (i <- (1 to 16 by 2))
    if(i%3 == 0) println(i)

3
9
15

Developing Functions

Let us understand how to develop functions using Scala as programming language.

    Function starts with def followed by function name.
    We need to specify data types for arguments or parameters.
    However, it is optional to use dot to invoke a function and circular brackets to pass arguments (with some restrictions).
    There is no need to specify return type and also return statement is optional for the function. If return statement need to be added, then we need to specify return type for the function.
    Functions which take another function as an argument is called higher order functions.

Tasks

Let us perform few tasks to understand how to develop functions in Scala.

    Sum of integers between lower bound and upper bound using formula.

def sumOfN(n: Int) = {
    (n * (n + 1))/2
}

sumOfN(10)

sumOfN: (n: Int)Int

55

def sumOfIntegers(lb: Int, ub: Int) = {
    sumOfN(ub) - sumOfN(lb -1)
}

sumOfIntegers(5, 10)

sumOfIntegers: (lb: Int, ub: Int)Int

45

    Sum of integers between lower bound and upper bound using loops.

def sumOfIntegers(lb: Int, ub: Int) = {
    var total = 0
    for (e <- (lb to ub))
        total += e
    total
}

sumOfIntegers(1, 10)

sumOfIntegers: (lb: Int, ub: Int)Int

55

    Sum of squares of integers between lower bound and upper bound using loops.

def sumOfSquares(lb: Int, ub: Int) = {
    var total = 0
    for (e <- (lb to ub))
        total += e * e
    total
}

sumOfSquares(2, 4)

sumOfSquares: (lb: Int, ub: Int)Int

29

    Sum of the even numbers between lower bound and upper bound using loops.

def sumOfEvens(lb: Int, ub: Int) = {
    var total = 0
    for (e <- (lb to ub))
        total += (if(e%2 == 0) e else 0)
    total
}

sumOfEvens(2, 4)

sumOfEvens: (lb: Int, ub: Int)Int

6

Anonymous Functions

Let us recap details related to Anonymous functions. In some programming languages like Python they are popularly known as lambda functions.

    We can develop functions with out names. They are called Anonymous Functions and also known as Lambda Functions.
    We typically use them to pass as arguments to higher order functions which takes functions as arguments

Tasks

Let us perform few tasks related to anonmous functions.

    Create a generic function mySum which is supposed to perform arithmetic using integers within a range.
        It takes 3 arguments - lb, ub and f.
        Function f should be invoked inside the function on each element within the range.

def mySum(lb: Int, ub: Int, f: Int => Int) = {
    var total = 0
    for (e <- (lb to ub))
        total += f(e)
    total
}

mySum: (lb: Int, ub: Int, f: Int => Int)Int

    Sum of integers between lower bound and upper bound using mySum.

mySum(2, 4, i => i)

9

    Sum of squares of integers between lower bound and upper bound using mySum.

mySum(2, 4, i => i * i)

29

    Sum of the even numbers between lower bound and upper bound using mySum.

mySum(2, 4, i => if(i%2 == 0) i else 0)

6

Overview of Collections and Tuples

Let's quickly recap about Collections and Tuples in Scala. We will primarily talk about collections and tuples that comes as part of Scala standard library such as List, Set, Map and tuple etc. There is no name for tuple type.

    Group of elements with length and index - List
    Group of unique elements - Set
    Group of key value pairs - Map
    While List, Set and Map contain group of homogeneous elements, tuple contains group of heterogeneous elements.
    We can consider List, Set and Map as a table in a database and tuple as a row or record in a given table.
    Typically we create list of tuples or set of tuples and Map is nothing but collection of tuples with 2 elements and keys are unique.
    We typically use Map Reduce APIs to process the data in collections. There are also some pre-defined functions such as size, sum, min, max etc for aggregating data in collections.

Tasks

Let us perform few tasks to quickly recap details about Collections and Tuples in Python. We will also quickly recap about Map Reduce APIs.

    Create a collection of orders by reading data from a file.

%%sh
ls -ltr /data/retail_db/orders/part-00000

import scala.io.Source

val ordersPath = "/data/retail_db/orders/part-00000"
val orders = Source.fromFile(ordersPath).
    getLines

ordersPath = /data/retail_db/orders/part-00000
orders = non-empty iterator

non-empty iterator

orders.size

68883

    Once iterated, the collection will be flushed out of memory. If you want to process again, you might have to read the data from the file again.
    This is the limitation of using Scala REPL or tools such as Jupyter Notebook using Scala REPL style features.

import scala.io.Source

val ordersPath = "/data/retail_db/orders/part-00000"
val orders = Source.fromFile(ordersPath).
    getLines

ordersPath = /data/retail_db/orders/part-00000
orders = non-empty iterator

non-empty iterator

orders.take(10).foreach(println)

1,2013-07-25 00:00:00.0,11599,CLOSED
2,2013-07-25 00:00:00.0,256,PENDING_PAYMENT
3,2013-07-25 00:00:00.0,12111,COMPLETE
4,2013-07-25 00:00:00.0,8827,CLOSED
5,2013-07-25 00:00:00.0,11318,COMPLETE
6,2013-07-25 00:00:00.0,7130,COMPLETE
7,2013-07-25 00:00:00.0,4530,COMPLETE
8,2013-07-25 00:00:00.0,2911,PROCESSING
9,2013-07-25 00:00:00.0,5657,PENDING_PAYMENT
10,2013-07-25 00:00:00.0,5648,PENDING_PAYMENT

    Get all unique order statuses. Make sure data is sorted in alphabetical order.

orders.map(order => order.split(",")(3)).toSet

Set(PAYMENT_REVIEW, CLOSED, SUSPECTED_FRAUD, PROCESSING, COMPLETE, PENDING, PENDING_PAYMENT, ON_HOLD, CANCELED)

    Get count of all unique dates.

import scala.io.Source

val ordersPath = "/data/retail_db/orders/part-00000"
val orders = Source.fromFile(ordersPath).
    getLines

orders.map(order => order.split(",")(1)).toSet.size

ordersPath = /data/retail_db/orders/part-00000
orders = empty iterator

364

    Sort the data in orders in ascending order by order_customer_id and then order_date.

import scala.io.Source

val ordersPath = "/data/retail_db/orders/part-00000"
val orders = Source.fromFile(ordersPath).
    getLines.
    toList

orders.
    sortBy(k => (k.split(",")(2).toInt, k.split(",")(1))).
    take(10).
    foreach(println)

22945,2013-12-13 00:00:00.0,1,COMPLETE
57963,2013-08-02 00:00:00.0,2,ON_HOLD
15192,2013-10-29 00:00:00.0,2,PENDING_PAYMENT
67863,2013-11-30 00:00:00.0,2,COMPLETE
33865,2014-02-18 00:00:00.0,2,COMPLETE
22646,2013-12-11 00:00:00.0,3,COMPLETE
61453,2013-12-14 00:00:00.0,3,COMPLETE
23662,2013-12-19 00:00:00.0,3,COMPLETE
35158,2014-02-26 00:00:00.0,3,COMPLETE
46399,2014-05-09 00:00:00.0,3,PROCESSING

ordersPath = /data/retail_db/orders/part-00000
orders = List(1,2013-07-25 00:00:00.0,11599,CLOSED, 2,2013-07-25 00:00:00.0,256,PENDING_PAYMENT, 3,2013-07-25 00:00:00.0,12111,COMPLETE, 4,2013-07-25 00:00:00.0,8827,CLOSED, 5,2013-07-25 00:00:00.0,11318,COMPLETE, 6,2013-07-25 00:00:00.0,7130,COMPLETE, 7,2013-07-25 00:00:00.0,4530,COMPLETE, 8,2013-07-25 00:00:00.0,2911,PROCESSING, 9,2013-07-25 00:00:00.0,5657,PENDING_PAYMENT, 10,2013-07-25 00:00:00.0,5648,PENDING_PAYMENT, 11,2013-07-25 00:00:00.0,918,PAYMENT_REVIEW, 12,2013-07-25 00:00:00.0,1837,CLOSED, 13,2013-07-25 00:00:00.0,9149,PENDING_PAYMENT, 14,2013-07-25 00:00:00.0,9842,PROCESSING, 15,2013-07-25 00:00:00.0,2568,COMPLETE, 16,2013-07-25 00:00:00.0,7276,PENDING_PAYMENT, 17,2013-07-25 00:00:...

List(1,2013-07-25 00:00:00.0,11599,CLOSED, 2,2013-07-25 00:00:00.0,256,PENDING_PAYMENT, 3,2013-07-25 00:00:00.0,12111,COMPLETE, 4,2013-07-25 00:00:00.0,8827,CLOSED, 5,2013-07-25 00:00:00.0,11318,COMPLETE, 6,2013-07-25 00:00:00.0,7130,COMPLETE, 7,2013-07-25 00:00:00.0,4530,COMPLETE, 8,2013-07-25 00:00:00.0,2911,PROCESSING, 9,2013-07-25 00:00:00.0,5657,PENDING_PAYMENT, 10,2013-07-25 00:00:00.0,5648,PENDING_PAYMENT, 11,2013-07-25 00:00:00.0,918,PAYMENT_REVIEW, 12,2013-07-25 00:00:00.0,1837,CLOSED, 13,2013-07-25 00:00:00.0,9149,PENDING_PAYMENT, 14,2013-07-25 00:00:00.0,9842,PROCESSING, 15,2013-07-25 00:00:00.0,2568,COMPLETE, 16,2013-07-25 00:00:00.0,7276,PENDING_PAYMENT, 17,2013-07-25 00:00:...

    Create a collection of order_items by reading data from a file.

import scala.io.Source

val orderItemsPath = "/data/retail_db/order_items/part-00000"
val orderItems = Source.fromFile(orderItemsPath).
    getLines

    Get revenue for a given order_item_order_id.

def getOrderRevenue(orderItems: List[String], orderId: Int) = {
    val orderItemsFiltered = orderItems.
        filter(orderItem => orderItem.split(",")(1).toInt == 2)
    val orderItemsMap = orderItemsFiltered.
        map(orderItem => orderItem.split(",")(4).toFloat)
    orderItemsMap.sum
}

getOrderRevenue: (orderItems: List[String], orderId: Int)Float

import scala.io.Source

val orderItemsPath = "/data/retail_db/order_items/part-00000"
val orderItems = Source.fromFile(orderItemsPath).
    getLines.
    toList

orderItemsPath = /data/retail_db/order_items/part-00000
orderItems = List(1,1,957,1,299.98,299.98, 2,2,1073,1,199.99,199.99, 3,2,502,5,250.0,50.0, 4,2,403,1,129.99,129.99, 5,4,897,2,49.98,24.99, 6,4,365,5,299.95,59.99, 7,4,502,3,150.0,50.0, 8,4,1014,4,199.92,49.98, 9,5,957,1,299.98,299.98, 10,5,365,5,299.95,59.99, 11,5,1014,2,99.96,49.98, 12,5,957,1,299.98,299.98, 13,5,403,1,129.99,129.99, 14,7,1073,1,199.99,199.99, 15,7,957,1,299.98,299.98, 16,7,926,5,79.95,15.99, 17,8,365,3,179.97,59.99, 18,8,365,5,299.95,59.99, 19,8,1014,4,199.92,49.98, 20,8,502,1,50.0,50.0, 21,9,191,2,199.98,99.99, 22,9,1073,1,199.99,199.99, 23,9,1073,1,199.99,199.99, 24,10,1073,1,199.99,199.99, 25,10,1014,2,99.96,49.98, 26,10,403,1,129.99,129.99, 27,10,917,1,21.99,21.99,...

List(1,1,957,1,299.98,299.98, 2,2,1073,1,199.99,199.99, 3,2,502,5,250.0,50.0, 4,2,403,1,129.99,129.99, 5,4,897,2,49.98,24.99, 6,4,365,5,299.95,59.99, 7,4,502,3,150.0,50.0, 8,4,1014,4,199.92,49.98, 9,5,957,1,299.98,299.98, 10,5,365,5,299.95,59.99, 11,5,1014,2,99.96,49.98, 12,5,957,1,299.98,299.98, 13,5,403,1,129.99,129.99, 14,7,1073,1,199.99,199.99, 15,7,957,1,299.98,299.98, 16,7,926,5,79.95,15.99, 17,8,365,3,179.97,59.99, 18,8,365,5,299.95,59.99, 19,8,1014,4,199.92,49.98, 20,8,502,1,50.0,50.0, 21,9,191,2,199.98,99.99, 22,9,1073,1,199.99,199.99, 23,9,1073,1,199.99,199.99, 24,10,1073,1,199.99,199.99, 25,10,1014,2,99.96,49.98, 26,10,403,1,129.99,129.99, 27,10,917,1,21.99,21.99,...

getOrderRevenue(orderItems, 2)

579.98

Limitations of Collections

We can use Collections for data processing. It provides rich APIs to read data from different sources, process the data and then write it to different targets.

    Scala Native Collections works well for light weight data processing.
    Collection operations are typically single threaded, which means only one process take care of processing the data.
    As data volume grows, the processing time might grow exponentially and also run into resource contention.
    It is not trivial to achieve desired parallelism using Scala Collections.
    There are Distributed Computing Frameworks such as Hadoop Map Reduce, Spark etc to take care of data processing at scale on multi node Hadoop or Spark Clusters.
    Both Hadoop Map Reduce and Spark comes with Distributed Computing Frameworks as well as APIs.

Scala Collections are typically used for light weight Data Processing and Spark is used for Data Processing at Scale.
Development Life Cycle

Let us understand the development life cycle. We typically use IDEs such as PyCharm to develop Python based applications.

    Create Project - retail
    Choose the interpreter 3.x
    Make sure plugins such as pandas are installed.
    Create config.py script for externalizing run time parameters such as input path, output path etc.
    Create app folder for the source code.

Tasks

Let us develop a simple application to understand end to end development life cycle.

    Read the data from order_items
    Get revenue for each order id
    Save the output which contain order id and revenue to a file.

Click here for the complete code for the above tasks.
Exercises

Let us perform few exercises to understand how to process the data. We will use LinkedIn data to perform some basic data processing using Python.

    Get LinkedIn archive.
        Go to https://linkedin.com
        Me on top -> Settings & Privacy
        Then go to "How LinkedIn users your data" -> Getting a copy of your data
        Register and download. You will get a link as part of the email.
    Data contain multiple CSV files. We will limit the analysis to Contacts.csv and Connections.csv.
    Get the number of contacts with out email ids.
    Get the number of contacts from each source.
    Get the number of connections with each title.
    Get the number of connections from each company.
    Get the number of contacts for each month in the year 2018.
    Use Postgres or MySQL as databases (you can setup in your laptop) and write connections data to the database.


