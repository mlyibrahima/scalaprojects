Data Processing - Overview
Pre-requisites and Module Introduction
Let us understand prerequisites before getting into the module.

Good understanding of Data Processing using Scala.
Data Processing Life Cycle
Reading Data from files
Processing Data using APIs
Writing Processed Data back to files
We can also use Databases as sources and sinks. It will be covered in relevant modules.
We can also read data in streaming fashion which is out of the scope of this course.
We will get an overview of the Data Processing Life Cycle by the end of the module.

Read data from the file.
Preview the schema and data to understand the characteristics of the data.
Get an overview of Data Frame APIs as well as functions used to process the data.
Check if there are any duplicates in the data.
Get an overview of how to write data in Data Frames to Files using File Formats such as Parquet using Compression.
We will deep dive into Data Frame APIs to process the data in subsequent modules.
Starting Spark Context
Let us understand more about Spark Context and also how to start it using SparkSession.

SparkSession is a class that is part of org.apache.spark.sql package.
When Spark application is submitted using spark-submit or spark-shell or pyspark, a web service called as Spark Context will be started.
Here is the example about how Spark Shell can be launched locally.
spark-shell \
  --master "local[*]"
Here is the example about how Spark Shell can be launched on multinode cluster such as our labs.
spark2-shell \
  --master yarn \
  --conf spark.ui.port=0
Make sure to understand the enviroment and use appropriate command to launch Spark Shell.
Spark Context maintains the context of all the jobs that are submitted until it is killed.
SparkSession is nothing but wrapper on top of Spark Context.
We need to first create SparkSession object with any name. But typically we use spark. Once it is created, several APIs will be exposed including read.
We need to at least set Application Name and also specify the execution mode in which Spark Context should run while creating SparkSession object.
We can use appName to specify name for the application and master to specify the execution mode.
Below is the sample code snippet which will start the Spark Session object for us.
import org.apache.spark.sql.SparkSession
val spark = SparkSession.
    builder.
    config("spark.ui.port", "0").
    appName("Data Processing - Overview").
    master("yarn").
    getOrCreate
spark
spark.sparkContext.getConf.getAll.foreach(println)
Overview of Spark read APIs
Let us get the overview of Spark read APIs to read files of different formats.

spark has a bunch of APIs to read data from files of different formats.
All APIs are exposed under spark.read
text - to read single column data from text files as well as reading each of the whole text file as one record.
csv- to read text files with delimiters. Default is a comma, but we can use other delimiters as well.
json - to read data from JSON files
orc - to read data from ORC files
parquet - to read data from Parquet files.
We can also read data from other file formats by plugging in and by using spark.read.format
We can also pass options based on the file formats. Go to Scala Docs which will be provided as part of the certification exam to get the list of options available.
inferSchema - to infer the data types of the columns based on the data.
header - to use header to get the column names in case of text files.
schema - to explicitly specify the schema.
Let us see an example about how to read delimited data from text files.
// spark.read.csv
val orders = spark.
    read.
    schema("""order_id INT, order_date TIMESTAMP,
              order_customer_id INT, order_status STRING
           """).
    csv("/public/retail_db/orders")
// spark.read.csv with option

val orders = spark.
    read.
    schema("""order_id INT, order_date TIMESTAMP,
              order_customer_id INT, order_status STRING
           """).
    option("sep", ",").
    csv("/public/retail_db/orders")
// spark.read.format

val orders = spark.
    read.
    schema("""order_id INT, order_date TIMESTAMP,
              order_customer_id INT, order_status STRING
           """).
    option("sep", ",").
    format("csv").
    load("/public/retail_db/orders")
Reading JSON data from text files. We can infer schema from the data as each JSON object contain both column name and value.
Example for JSON
{ "order_id": 1, "order_date": "2013-07-25 00:00:00.0", "order_customer_id": 12345, "order_status": "COMPLETE" }
// spark.read.json

val orders = spark.
    read.
    option("inferSchema", "false").
    schema("""order_id INT, order_date TIMESTAMP,
              order_customer_id INT, order_status STRING
           """).
    json("/public/retail_db_json/orders")
// spark.read.format

val orders = spark.
    read.
    option("inferSchema", "false").
    schema("""order_id INT, order_date TIMESTAMP,
              order_customer_id INT, order_status STRING
           """).
    format("json").
    load("/public/retail_db_json/orders")
Previewing Schema and Data
Here are the APIs that can be used to preview the schema and data.

printSchema can be used to get the schema details.
show can be used to preview the data. It will typically show first 20 records where output is truncated.
describe can be used to get statistics out of our data.
We can pass number of records and set truncate to false while previewing the data.
val orders = spark.
    read.
    schema("""order_id INT, 
              order_date STRING, 
              order_customer_id INT, 
              order_status STRING
           """
          ).
    csv("/public/retail_db/orders")
orders
// Print Schema
orders.printSchema
// Describe
orders.describe().show(false)
// Preview Data - Default
orders.show
// Preview Data - 10, with truncate false
orders.show(10, truncate=false)
Overview of Data Frame APIs
Let us get an overview of Data Frame APIs to process data in Data Frames.

Row Level Transformations or Projection of Data can be done using select, selectExpr, withColumn, drop on Data Frame.
We typically apply functions from org.apache.spark.sql.functions on columns using select and withColumn
Filtering is typically done either by using filter or where on Data Frame.
We can pass the condition to filter or where either by using SQL Style or Programming Language Style.
Global Aggregations can be performed directly on the Data Frame.
By Key or Grouping Aggregations are typically performed using groupBy and then aggregate functions using agg
We can sort the data in Data Frame using sort or orderBy
We will talk about Window Functions later. We can use use Window Functions for some advanced Aggregations and Ranking.
Tasks
Let us understand how to project the data using different options such as select, selectExpr, withColumn, drop.

Create Dataframe employees using Collection
val employees = List((1, "Scott", "Tiger", 1000.0, "united states"),
                     (2, "Henry", "Ford", 1250.0, "India"),
                     (3, "Nick", "Junior", 750.0, "united KINGDOM"),
                     (4, "Bill", "Gomes", 1500.0, "AUSTRALIA")
                    )
val employeesDF = employees.
    toDF("employee_id", 
         "first_name", 
         "last_name", 
         "salary", 
         "nationality"
        )
employeesDF.printSchema
employeesDF.show
Project employee first name and last name.
employeesDF.select("first_name", "last_name").show
Project all the fields except for Nationality
employeesDF.drop("nationality").show
We will explore most of the APIs to process data in Data Frames as we get into the data processing at a later point in time

Overview of Functions
Let us get an overview of different functions that are available to process data in columns.

While Data Frame APIs work on the Data Frame, at times we might want to apply functions on column values.
Functions to process column values are available under org.apache.spark.sql.functions. These are typically used in select or withColumn on top of Data Frame.
There are approximately 300 pre-defined functions available for us.
Some of the important functions can be broadly categorized into String Manipulation, Date Manipulation, Numeric Functions and Aggregate Functions.
String Manipulation Functions
Concatenating Strings - concat
Getting Length - length
Trimming Strings - trim,rtrim, ltrim
Padding Strings - lpad, rpad
Extracting Strings - split, substring
Date Manipulation Functions
Date Arithmetic - date_add, date_sub, datediff, add_months
Date Extraction - dayofmonth, month, year, date_format
Get beginning period - trunc, date_trunc
Numeric Functions - abs, greatest
Aggregate Functions - sum, min, max
There are some special functions such as col, lit etc.
col is used to convert string to column type. It can also be invoked using $ after importing spark.implicits._
lit is used to convert a literal to column value so that it can be used to generate derived fields by manipulating using literals.
Tasks
Let us perform a task to understand how functions are typically used.

Project full name by concatenating first name and last name along with other fields excluding first name and last name.
// Using col and lit
import org.apache.spark.sql.functions.{col, lit, concat}
employeesDF.
    select(col("employee_id"),
           concat(col("first_name"), lit(" "), col("last_name")).alias("full_name"),
           col("salary"),
           col("nationality")
          ).
    show
employeesDF.
    withColumn("full_name", 
               concat(col("first_name"), lit(", "), col("last_name"))).
    drop("first_name", "last_name").
    show
val spark = SparkSession.
    builder.
    config("spark.ui.port", "0").
    appName("Data Processing - Overview").
    master("yarn").
    getOrCreate
// Using $ and lit
import spark.implicits._
employeesDF.
    withColumn("full_name", 
               concat($"first_name", lit(", "), $"last_name")).
    drop("first_name", "last_name").
    show
spark.sql("SHOW functions").show(300, false)
spark.sql("DESCRIBE FUNCTION concat").show(false)
// Using SQL Style
employeesDF.
    selectExpr("employee_id",
               "concat(first_name, ' ', last_name) AS full_name",
               "salary", 
               "nationality"
              ).
    show
We will explore most of the functions as we get into the data processing at a later point in time

Overview of Spark Write APIs
Let us understand how we can write Data Frames to different file formats.

All the batch write APIs are grouped under write which is exposed to Data Frame objects.
All APIs are exposed under spark.read
text - to write single column data to text files.
csv - to write to text files with delimiters. Default is a comma, but we can use other delimiters as well.
json - to write data to JSON files
orc - to write data to ORC files
parquet - to write data to Parquet files.
We can also write data to other file formats by plugging in and by using write.format, for example avro
We can use options based on the type using which we are writing the Data Frame to.
compression - Compression codec (gzip, snappy etc)
sep - to specify delimiters while writing into text files using csv
We can overwrite the directories or append to existing directories using mode
Create copy of orders data in parquet file format with no compression. If the folder already exists overwrite it. Target Location: /user/[YOUR_USER_NAME]/retail_db/orders
When you pass options, if there are typos then options will be ignored rather than failing. Be careful and make sure that output is validated.
By default the number of files in the output directory is equal to number of tasks that are used to process the data in the last stage. However, we might want to control number of files so that we don"t run into too many small files issue.
We can control number of files by using coalesce. It has to be invoked on top of Data Frame before invoking write.
val orders = spark.
    read.
    schema("""order_id INT, 
              order_date STRING, 
              order_customer_id INT, 
              order_status STRING
           """
          ).
    csv("/public/retail_db/orders")
orders = [order_id: int, order_date: string ... 2 more fields]
[order_id: int, order_date: string ... 2 more fields]
spark.conf.get("spark.sql.parquet.compression.codec")
snappy
// Using write.parquet
orders.
    write.
    mode("overwrite").
    option("compression", "none").
    parquet("/user/training/retail_db/orders")
// Using write.format("parquet")
orders.
    coalesce(1).
    write.
    mode("overwrite").
    option("compression", "none").
    format("parquet").
    save("/user/training/retail_db/orders")
import sys.process._

"hdfs dfs -ls /user/training/retail_db/orders" !
Found 2 items
-rw-r--r--   2 training training          0 2020-04-07 20:56 /user/training/retail_db/orders/_SUCCESS
-rw-r--r--   2 training training     495141 2020-04-07 20:56 /user/training/retail_db/orders/part-00000-4e6cfdeb-f1c9-4758-bff3-0fe28ca342f3-c000.parquet
warning: there was one feature warning; re-run with -feature for details
0
spark.read.parquet("/user/training/retail_db/orders").printSchema
root
 |-- order_id: integer (nullable = true)
 |-- order_date: string (nullable = true)
 |-- order_customer_id: integer (nullable = true)
 |-- order_status: string (nullable = true)

spark.read.parquet("/user/training/retail_db/orders").show
+--------+--------------------+-----------------+---------------+
|order_id|          order_date|order_customer_id|   order_status|
+--------+--------------------+-----------------+---------------+
|       1|2013-07-25 00:00:...|            11599|         CLOSED|
|       2|2013-07-25 00:00:...|              256|PENDING_PAYMENT|
|       3|2013-07-25 00:00:...|            12111|       COMPLETE|
|       4|2013-07-25 00:00:...|             8827|         CLOSED|
|       5|2013-07-25 00:00:...|            11318|       COMPLETE|
|       6|2013-07-25 00:00:...|             7130|       COMPLETE|
|       7|2013-07-25 00:00:...|             4530|       COMPLETE|
|       8|2013-07-25 00:00:...|             2911|     PROCESSING|
|       9|2013-07-25 00:00:...|             5657|PENDING_PAYMENT|
|      10|2013-07-25 00:00:...|             5648|PENDING_PAYMENT|
|      11|2013-07-25 00:00:...|              918| PAYMENT_REVIEW|
|      12|2013-07-25 00:00:...|             1837|         CLOSED|
|      13|2013-07-25 00:00:...|             9149|PENDING_PAYMENT|
|      14|2013-07-25 00:00:...|             9842|     PROCESSING|
|      15|2013-07-25 00:00:...|             2568|       COMPLETE|
|      16|2013-07-25 00:00:...|             7276|PENDING_PAYMENT|
|      17|2013-07-25 00:00:...|             2667|       COMPLETE|
|      18|2013-07-25 00:00:...|             1205|         CLOSED|
|      19|2013-07-25 00:00:...|             9488|PENDING_PAYMENT|
|      20|2013-07-25 00:00:...|             9198|     PROCESSING|
+--------+--------------------+-----------------+---------------+
only showing top 20 rows

Conclusion
Let us recap about key takeaways from this module.

APIs to read the data from files into Data Frame.
Previewing Schema and the data in Data Frame.
Overview of Data Frame APIs and Functions
Writing data from Data Frame into Files
Reorganizing the airlines data by month
Simple APIs to analyze the data.
Now it is time for us to deep dive into APIs to perform all the standard transformations as part of Data Processing.

