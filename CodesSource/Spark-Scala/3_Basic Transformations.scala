
Basic Transformations
Overview of Basic Transformations

We will cover filtering, aggregations and sorting as part of this module and look into joins and ranking in subsequent modules.

Let us define problem statements and come up with solutions to learn more about Data Frame APIs.

    Get total number of flights as well as number of flights which are delayed in departure and number of flights delayed in arrival.
        Output should contain 3 columns - FlightCount, DepDelayedCount, ArrDelayedCount
    Get number of flights which are delayed in departure and number of flights delayed in arrival for each day along with number of flights departed for each day.
        Output should contain 4 columns - FlightDate, FlightCount, DepDelayedCount, ArrDelayedCount
        FlightDate should be of YYYY-MM-dd format.
        Data should be sorted in ascending order by flightDate

Starting Spark Context

Let us start spark context for this Notebook so that we can execute the code provided.

import org.apache.spark.sql.SparkSession

val spark = SparkSession.
    builder.
    config("spark.ui.port", "0").
    appName("Basic Transformations").
    master("yarn").
    getOrCreate

Overview of Filtering

Let us understand few important details related to filtering before we get into the solution

val airlines_path = "/public/airlines_all/airlines-part/flightmonth=200801"

import sys.process._

"hdfs dfs -ls /public/airlines_all/airlines-part/flightmonth=200801" !

val airlines = spark.
    read.
    parquet(airlines_path)

airlines.printSchema

    Filtering can be done either by using filter or where. These are like synonyms to each other.
    When it comes to the condition, we can either pass it in SQL Style or Data Frame Style.
    Example for SQL Style - airlines.filter("IsArrDelayed = 'YES'").show() or airlines.where("IsArrDelayed = 'YES'").show()
    Example for Data Frame Style - airlines.filter(airlines("IsArrDelayed") === "YES").show() or airlines.filter($"IsArrDelayed" === "YES").show(). We can also use where instead of filter.
    Here are the other operations we can perform to filter the data - !=, >, <, >=, <=, LIKE, BETWEEN with AND
    If we have to validate against multiple columns then we need to use boolean operations such as AND and OR.
    If we have to compare each column value with multiple values then we can use the IN operator.

Tasks

Let us perform some tasks to understand filtering in detail. Solve all the problems by passing conditions using both SQL Style as well as API Style.

    Read the data for the month of 2008 January.

val airlines_path = "/public/airlines_all/airlines-part/flightmonth=200801"

val airlines_all = spark.
    read.
    parquet(airlines_path)

airlines_all.printSchema

val airlines = airlines_all.
    select("Year", "Month", "DayOfMonth",
           "DepDelay", "ArrDelay", "UniqueCarrier", 
           "FlightNum", "IsArrDelayed", "IsDepDelayed"
          )

airlines.show

Get count of flights which are departed late at origin and reach destination early or on time.

airlines.show

    SQL Style

airlines.
    filter("IsDepDelayed = 'YES' AND IsArrDelayed = 'NO'").
    count

    API Style

import org.apache.spark.sql.functions.col

airlines.
    filter(col("IsDepDelayed") === "YES" and col("IsArrDelayed") === "NO").
    count

import spark.implicits._

airlines.
    filter($"IsDepDelayed" === "YES" and $"IsArrDelayed" === "NO").
    count

airlines.
    filter(airlines("IsDepDelayed") === "YES" and airlines("IsArrDelayed") === "NO").
    count

Get count of flights which are departed late from origin by more than 60 minutes.

    SQL Style

airlines.
    filter("DepDelay > 60").
    count

    API Style

import org.apache.spark.sql.functions.col

airlines.
    filter(col("DepDelay") > 60).
    count

import spark.implicits._

airlines.
    filter($"DepDelay" > 60).
    count

airlines.
    filter(airlines("DepDelay") > 60).
    count

Get count of flights which are departed early or on time but arrive late by at least 15 minutes.

    SQL Style

airlines.
    filter("IsDepDelayed = 'NO' AND ArrDelay >= 15").
    count

    API Style

import org.apache.spark.sql.functions.col

airlines.
    filter(col("IsDepDelayed") === "NO" and col("ArrDelay") >= 15).
    count

import spark.implicits._

airlines.
    filter($"IsDepDelayed" === "NO" and $"ArrDelay" >= 15).
    count

airlines.
    filter(airlines("IsDepDelayed") === "NO" and airlines("ArrDelay") >= 15).
    count

Get count of flights departed from following major airports - ORD, DFW, ATL, LAX, SFO.

    SQL Style

airlines_all.
    filter("Origin IN ('ORD', 'DFW', 'ATL', 'LAX', 'SFO')").
    select("Origin").
    distinct.
    show

airlines_all.
    filter("Origin IN ('ORD', 'DFW', 'ATL', 'LAX', 'SFO')").
    count

    API Style

import org.apache.spark.sql.functions.col

airlines_all.
    filter(col("Origin") isin ("ORD", "DFW", "ATL", "LAX", "SFO")).
    count

import spark.implicits._

airlines_all.
    filter($"Origin" isin ("ORD", "DFW", "ATL", "LAX", "SFO")).
    count

airlines_all.
    filter(airlines_all("Origin") isin ("ORD", "DFW", "ATL", "LAX", "SFO")).
    count

Get count of flights departed late between 2008 January 1st to January 9th using FlightDate.

    Date should be of yyyyMMdd format.

airlines.show

    Add a column FlightDate by using Year, Month and DayOfMonth. Format should be yyyyMMdd.

import org.apache.spark.sql.functions.{lpad, concat, col}

airlines.
    withColumn("FlightDate", 
               concat(col("Year"), 
                      lpad(col("Month"), 2, "0"), 
                      lpad(col("DayOfMOnth"), 2, "0")
                     )
              ).
    show

    SQL Style

airlines.
    withColumn("FlightDate", 
               concat(col("Year"), 
                      lpad(col("Month"), 2, "0"), 
                      lpad(col("DayOfMOnth"), 2, "0")
                     )
              ).
    filter("FlightDate LIKE '2008010%' AND IsDepDelayed = 'YES'").
    count

airlines.
    withColumn("FlightDate", 
               concat(col("Year"), 
                      lpad(col("Month"), 2, "0"), 
                      lpad(col("DayOfMOnth"), 2, "0")
                     )
              ).
    filter("FlightDate BETWEEN '20080101' AND '20080109' AND IsDepDelayed = 'YES'").
    count

    API Style

airlines.
    withColumn("FlightDate", 
               concat(col("Year"), 
                      lpad(col("Month"), 2, "0"), 
                      lpad(col("DayOfMOnth"), 2, "0")
                     )
              ).
    filter($"FlightDate" like "2008010%" and $"IsDepDelayed" === "YES").
    count

airlines.
    withColumn("FlightDate", 
               concat(col("Year"), 
                      lpad(col("Month"), 2, "0"), 
                      lpad(col("DayOfMOnth"), 2, "0")
                     )
              ).
    filter($"FlightDate" between ("20080101", "20080109") and $"IsDepDelayed" === "YES").
    count

Get number of flights departed late on Sundays.

val l = List("X")

import spark.implicits._

val df = l.toDF("dummy")

import org.apache.spark.sql.functions.current_date

df.select(current_date).show

import org.apache.spark.sql.functions.date_format

df.select(current_date, date_format(current_date, "EEEE")).show

    SQL Style

import org.apache.spark.sql.functions.to_date

airlines.
    withColumn("FlightDate", 
               concat(col("Year"), 
                      lpad(col("Month"), 2, "0"), 
                      lpad(col("DayOfMOnth"), 2, "0")
                     )
              ).
    filter("date_format(to_date(FlightDate, 'yyyyMMdd'), 'EEEE') = 'Sunday' AND IsDepDelayed = 'YES'").
    count

    API Style

airlines.
    withColumn("FlightDate", 
               concat(col("Year"), 
                      lpad(col("Month"), 2, "0"), 
                      lpad(col("DayOfMOnth"), 2, "0")
                     )
              ).
    filter(date_format(to_date($"FlightDate", "yyyyMMdd"), "EEEE") === "Sunday" and $"IsDepDelayed" === "YES").
    count

Overview of Aggregations

Let us go through the details related to aggregations using Spark.

    We can perform total aggregations directly on Dataframe or we can perform aggregations after grouping by a key(s).
    Here are the APIs which we typically use to group the data using a key.
        groupBy
        rollup
        cube
    Here are the functions which we typically use to perform aggregations.
        count
        sum, avg
        min, max
    If we want to provide aliases to the aggregated fields then we have to use agg after groupBy.
    Let us get the count of flights for each day for the month of 200801.

val airlines_path = "/public/airlines_all/airlines-part/flightmonth=200801"

val airlines = spark.
    read.
    parquet(airlines_path)

import org.apache.spark.sql.functions.{lpad, concat, count, lit}

import spark.implicits._

airlines.
    groupBy(concat(
        $"Year", 
        lpad($"Month", 2, "0"), 
        lpad($"DayOfMonth", 2, "0")).alias("FlightDate")
    ).
    agg(count(lit(1)).alias("FlightCount")).
    show

Overview of Sorting

Let us understand how to sort the data in a Data Frame.

    We can use orderBy or sort to sort the data.
    We can perform composite sorting by passing multiple columns or expressions.
    By default data is sorted in ascending order, we can change it to descending by applying desc() function on the column or expression.
    Let us sort the Flight Count for each day for the month of 2008 January in descending order by count

val flightCountDaily = airlines.
    groupBy(concat(
        $"Year", 
        lpad($"Month", 2, "0"), 
        lpad($"DayOfMonth", 2, "0")).alias("FlightDate")
    ).
    agg(count(lit(1)).alias("FlightCount"))

flightCountDaily.orderBy($"FlightCount".desc).show

Solutions - Problem 1

Get total number of flights as well as number of flights which are delayed in departure and number of flights delayed in arrival.

    Output should contain 3 columns - FlightCount, DepDelayedCount, ArrDelayedCount

Reading airlines data

val airlines_path = "/public/airlines_all/airlines-part/flightmonth=200801"

val airlines_all = spark.
    read.
    parquet(airlines_path)

val airlines = airlines_all.
    select("Year", "Month", "DayOfMonth",
           "DepDelay", "ArrDelay", "UniqueCarrier", 
           "FlightNum", "IsArrDelayed", "IsDepDelayed"
          )

airlines.printSchema

airlines.show

Get flights with delayed arrival

//SQL Style
airlines.filter("IsArrDelayed = 'YES'").show

// API Style
airlines.filter(airlines("IsArrDelayed") === "YES").show

import org.apache.spark.sql.functions.col

airlines.filter(col("IsArrDelayed") === "YES").show

import spark.implicits._

airlines.filter($"IsArrDelayed" === "YES").show

Get delayed counts

// Departure Delayed Count
airlines.
    filter(airlines("IsDepDelayed") === "YES").
    count

// Arrival Delayed Count
airlines.
    filter(airlines("IsArrDelayed") === "YES").
    count

Final Solution

import org.apache.spark.sql.functions.{col, lit, count, sum, expr}

airlines.agg(count(lit(1)).alias("FlightCount"),
             sum(expr("CASE WHEN IsDepDelayed = 'YES' THEN 1 ELSE 0 END")).alias("DepDelayedCount"),
             sum(expr("CASE WHEN IsArrDelayed = 'YES' THEN 1 ELSE 0 END")).alias("ArrDelayedCount")
            ).show

Solutions - Problem 2

Get number of flights which are delayed in departure and number of flights delayed in arrival for each day along with number of flights departed for each day.

    Output should contain 4 columns - FlightDate, FlightCount, DepDelayedCount, ArrDelayedCount
    FlightDate should be of yyyy-MM-dd format.
    Data should be sorted in ascending order by flightDate

val airlines_path = "/public/airlines_all/airlines-part/flightmonth=200801"

val airlines_all = spark.
    read.
    parquet(airlines_path)

val airlines = airlines_all.
    select("Year", "Month", "DayOfMonth",
           "DepDelay", "ArrDelay", "UniqueCarrier", 
           "FlightNum", "IsArrDelayed", "IsDepDelayed"
          )

airlines.printSchema

airlines.show

Grouping Data by Flight Date

import org.apache.spark.sql.functions.{lit, col, lpad, concat}

airlines.
    groupBy(concat($"Year", 
                   lit("-"),
                   lpad($"Month", 2, "0"), 
                   lit("-"),
                   lpad($"DayOfMonth", 2, "0")
                  )
           )

Getting Counts by Flight Date

airlines.
    groupBy(concat($"Year", 
                   lit("-"),
                   lpad($"Month", 2, "0"), 
                   lit("-"),
                   lpad($"DayOfMonth", 2, "0")
                  ).alias("FlightDate")
           ).
    count.
    show

airlines.
    filter("IsDepDelayed = 'YES'").
    groupBy(concat($"Year", 
                   lit("-"),
                   lpad($"Month", 2, "0"), 
                   lit("-"),
                   lpad($"DayOfMonth", 2, "0")
                  ).alias("FlightDate")
           ).
    count.
    show

airlines.
    filter("IsArrDelayed = 'YES'").
    groupBy(concat($"Year", 
                   lit("-"),
                   lpad($"Month", 2, "0"), 
                   lit("-"),
                   lpad($"DayOfMonth", 2, "0")
                  ).alias("FlightDate")
           ).
    count.
    show

Getting total as well as delayed counts for each day

import org.apache.spark.sql.functions.{sum, count, expr}

airlines.
    groupBy(concat($"Year", 
                   lit("-"),
                   lpad($"Month", 2, "0"), 
                   lit("-"),
                   lpad($"DayOfMonth", 2, "0")
                  ).alias("FlightDate")
           ).
    agg(count(lit(1)).alias("FlightCount"),
        sum(expr("CASE WHEN IsDepDelayed = 'YES' THEN 1 ELSE 0 END")).alias("DepDelayedCount"),
        sum(expr("CASE WHEN IsArrDelayed = 'YES' THEN 1 ELSE 0 END")).alias("ArrDelayedCount")
       ).
    show

Sorting Data By FlightDate

airlines.
    groupBy(concat($"Year", 
                   lit("-"),
                   lpad($"Month", 2, "0"), 
                   lit("-"),
                   lpad($"DayOfMonth", 2, "0")
                  ).alias("FlightDate")
           ).
    agg(count(lit(1)).alias("FlightCount"),
        sum(expr("CASE WHEN IsDepDelayed = 'YES' THEN 1 ELSE 0 END")).alias("DepDelayedCount"),
        sum(expr("CASE WHEN IsArrDelayed = 'YES' THEN 1 ELSE 0 END")).alias("ArrDelayedCount")
       ).
    orderBy("FlightDate").
    show(31)


