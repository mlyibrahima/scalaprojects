Getting help or usage¶

Let us explore details about how to list the commands and get the help or usage for given command.

    Even though we can run commands from almost all the nodes in the clusters, we should only use 
    Gateway to run HDFS Commands.

    First we need to make sure designated Gateway server is Gateway for HDFS service so that we 
    can run commands from Gateway node. In our case we have designated 
    gw02.itversity.com or gw03.itversity.com as Gateways.

    Typically Namenode process will be running on port number 8020. 
    We can also pass namenode URI to access HDFS.

%%sh

head -20 /etc/hadoop/conf/core-site.xml 
- or cat /etc/hadoop/conf/core-site.xml

<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://m01.itversity.com:9000</value>
    </property>
    <property>
        <name>fs.trash.interval</name>
        <value>86400</value>              
    </property>
    <property>
        <name>hadoop.proxyuser.hue.hosts</name>
        <value>*</value>
    </property>
    <property>
        <name>hadoop.proxyuser.hue.groups</name>
        <value>*</value>
    </property>
    <property>
        <name>hadoop.proxyuser.hdfs.groups</name>
        <value>*</value>
    </property>
    <property>
        <name>hadoop.proxyuser.hdfs.hosts</name>
        <value>*</value>
    </property>
    <property>
        <name>hadoop.proxyuser.hive.groups</name>
        <value>*</value>
    </property>
    <property>
        <name>hadoop.proxyuser.hive.hosts</name>
        <value>*</value>
    </property>
</configuration>[itv001573@g01 ~]$ 

%%sh

hdfs dfs -ls /user/${USER}

%%sh

hdfs dfs -ls hdfs://g01.itversity.com:8020/user/${USER}

hdfs dfs -ls hdfs://m01.itversity.com:9000/user/${USER} --for me


    hadoop fs or hdfs dfs – list all the commands available

    hadoop fs -usage – will give us basic usage for given command

    hadoop fs -help – will give us additional information for all the commands. 
    It is same as just running hadoop fs or hdfs dfs.

    We can run help on individual commands as well - example: hadoop fs -help ls or hdfs dfs -help ls

%%sh

hdfs dfs -help

%%sh

hdfs dfs -usage ls

%%sh

hdfs dfs -help ls

%%sh

hdfs dfs -ls /public/retail_db


Listing HDFS Files¶

Now let us walk through different options we have with hdfs ls command to list the files.

    We can get usage by running hdfs dfs -usage ls.

%%sh

hdfs dfs -usage ls

    We can get help using hdfs dfs -help ls

%%sh

hdfs dfs -help ls

    Let us list all the files in /public/nyse_all/nyse_data folder. It is one of the public data sets that are available under /public. By default files and folders are sorted in ascending order by name.

%%sh

hdfs dfs -ls /public/nyse_all/nyse_data

%%sh

hdfs dfs -ls -r /public/nyse_all/nyse_data

    We can sort the files and directories by time using -t option. By default you will see latest files at top. We can reverse it by using -t -r.

%%sh

hdfs dfs -ls -t /public/nyse_all/nyse_data

%%sh

hdfs dfs -ls -t -r /public/nyse_all/nyse_data

    We can sort the files and directories by size using -S. By default, the files will be sorted in descending order by size. We can reverse the sorting order using -S -r.

%%sh

hdfs dfs -ls -S /public/nyse_all/nyse_data

%%sh

hdfs dfs -ls -S -r /public/nyse_all/nyse_data

%%sh

hdfs dfs -ls -h /public/nyse_all/nyse_data

%%sh

hdfs dfs -ls -h -t /public/nyse_all/nyse_data

%%sh

hdfs dfs -ls -h -S /public/nyse_all/nyse_data

%%sh

hdfs dfs -ls -h -S -r /public/nyse_all/nyse_data


Managing HDFS Directories¶

Now let us have a look at how to create directories and manage ownership.

    By default hdfs is superuser of HDFS

    hadoop fs -mkdir or hdfs dfs -mkdir – to create directories

    hadoop fs -chown or hdfs dfs -chown – to change ownership of files

    chown can also be used to change the group. We can change the group using -chgrp command as well. Make sure to run -help on chgrp and check the details.

    Here are the steps to create user space. Only users in HDFS group can take care of it.

        Create directory with user id itversity under /user

        Change ownership to the same name as the directory created earlier (/user/itversity)

        You can validate permissions by using hadoop fs -ls or hdfs dfs -ls command on /user. Make sure to grep for the user name you are looking for.

    Let’s go ahead and create user space in HDFS for itversity. I have to login as sudoer and run below commands.

sudo -u hdfs hdfs dfs -mkdir /user/itversity1
sudo -u hdfs hdfs dfs -chown -R itversity:students /user/itversity
hdfs dfs -ls /user|grep itversity

    You should be able to create folders under your home directory.

%%sh

hdfs dfs -ls /user/${USER}

%%sh

hdfs dfs -mkdir /user/${USER}/retail_db

%%sh

hdfs dfs -ls /user/${USER}

    You can create the directory structure using mkdir -p. The existing folders will be ignored and non existing folders will be created.

        Let us run hdfs dfs -mkdir -p /user/${USER}/retail_db/orders/year=2020.

        As /user/${USER}/retail_db already exists, it will be ignored.

        Both /user/${USER}/retail_db/orders as well as /user/${USER}/retail_db/orders/year=2020 will be created.

%%sh

hdfs dfs -help mkdir

%%sh

hdfs dfs -ls -R /user/${USER}/retail_db

%%sh

hdfs dfs -mkdir -p /user/${USER}/retail_db/orders/year=2020

%%sh

hdfs dfs -ls -R /user/${USER}/retail_db

    We can delete non empty directory using hdfs dfs -rm -R and empty directory using hdfs dfs -rmdir. We will explore hdfs dfs -rm in detail later.

%%sh

hdfs dfs -help rmdir

%%sh

hdfs dfs -rmdir /user/${USER}/retail_db/orders/year=2020

%%sh

hdfs dfs -rm /user/${USER}/retail_db

%%sh

hdfs dfs -rmdir /user/${USER}/retail_db

%%sh

hdfs dfs -rm -R /user/${USER}/retail_db

%%sh

hdfs dfs -ls /user/${USER}


Copying files from local to HDFS¶

We can copy files from local file system to HDFS either by using copyFromLocal or put command.

    hdfs dfs -copyFromLocal or hdfs dfs -put – to copy files or directories from local filesystem into HDFS. We can also use hadoop fs in place of hdfs dfs.

    However, we will not be able to update or fix data in files when they are in HDFS. If we have to fix any data, we have to move file to local file system, fix data and then copy back to HDFS.

    Files will be divided into blocks and will be stored on Datanodes in distributed fashion based on block size and replication factor. We will get into the details later.

test

%%sh

hdfs dfs -ls /user/${USER}

%%sh

hdfs dfs -mkdir /user/${USER}/retail_db

%%sh

hdfs dfs -ls /user/${USER}

%%sh

hdfs dfs -ls /user/${USER}/retail_db

%%sh

hdfs dfs -help put

%%sh

hdfs dfs -help copyFromLocal

Warning

This will copy the entire folder to /user/${USER}/retail_db and you will see /user/${USER}/retail_db/retail_db. You can use the next command to get files as expected.

%%sh

ls -ltr /data/retail_db

%%sh

hdfs dfs -put /data/retail_db /user/${USER}/retail_db

%%sh

hdfs dfs -ls /user/${USER}/retail_db

%%sh

hdfs dfs -ls /user/${USER}/retail_db/retail_db

Note

Let’s drop this folder and make sure files are copied as expected. As the folder is pre-created, we can use patterns to copy the sub folders.

%%sh

hdfs dfs -help rm

%%sh

hdfs dfs -rm -R -skipTrash /user/${USER}/retail_db/retail_db

%%sh

hdfs dfs -ls /user/${USER}/retail_db/

%%sh

hdfs dfs -put /data/retail_db/order* /user/${USER}/retail_db

%%sh

hdfs dfs -ls /user/${USER}/retail_db/

%%sh

hdfs dfs -put -f /data/retail_db/* /user/${USER}/retail_db

%%sh

hdfs dfs -ls /user/${USER}/retail_db/

%%sh

hdfs dfs -ls -R /user/${USER}/retail_db/

Note

Alternatively you can use copyFromLocal as well.

%%sh

hdfs dfs -rm -R -skipTrash /user/${USER}/retail_db

%%sh

hdfs dfs -mkdir /user/${USER}/retail_db

%%sh

hdfs dfs -ls /user/itversity/retail_db/

%%sh

hdfs dfs -copyFromLocal /data/retail_db/* /user/${USER}/retail_db

%%sh

hdfs dfs -ls /user/${USER}/retail_db

Note

We can also use this alternative approach to directly copy the folder /data/retail_db to /user/${USER}/retail_db. Let us first delete /user/${USER}/retail_db using skipTrash.

%%sh

hdfs dfs -rm -R -skipTrash /user/${USER}/retail_db

Note

We can specify the target location as /user/${USER}. It will create the retail_db folder and its contents.

%%sh

hdfs dfs -put /data/retail_db /user/${USER}

%%sh

hdfs dfs -ls /user/${USER}/retail_db

    If we try to run hdfs dfs -put /data/retail_db /user/${USER} again it will fail as the target folder already exists.

%%sh

hdfs dfs -put /data/retail_db /user/${USER}

    We can use -f as part of put or copyFromLocal to replace existing folder.

%%sh

hdfs dfs -put -f /data/retail_db /user/${USER}

%%sh

hdfs dfs -ls /user/${USER}/retail_db

%%sh

hdfs dfs -ls -R /user/${USER}/retail_db


Copying files from HDFS to Local¶

We can copy files from HDFS to local file system either by using copyToLocal or get command.

    hdfs dfs -copyToLocal or hdfs dfs -get – to copy files or directories from HDFS to local filesystem.

    It will read all the blocks using index in sequence and construct the file in local file system.

    If the target file or directory already exists in the local file system, get will fail saying already exists

%%sh

hdfs dfs -help get

%%sh

hdfs dfs -help copyToLocal

Warning

This will copy the entire folder from /user/${USER}/retail_db to local home directory and you will see /home/${USER}/retail_db.

%%sh

hdfs dfs -ls /user/${USER}/retail_db

%%sh

ls -ltr /home/${USER}/

%%sh

mkdir /home/${USER}/retail_db

%%sh

hdfs dfs -get /user/${USER}/retail_db/* /home/${USER}/retail_db

%%sh

ls -ltr /home/${USER}/retail_db

Note

This will fail as retail_db folder already exists.

%%sh

hdfs dfs -get /user/${USER}/retail_db /home/${USER}

Note

Alternative approach, where the folder and contents are copied directly.

%%sh

rm -rf /home/${USER}/retail_db

%%sh

ls -ltr /home/${USER}

%%sh

hdfs dfs -get /user/${USER}/retail_db /home/${USER}

%%sh

ls -ltr /home/${USER}/retail_db/*

    We can also use patterns while using get command to get files from HDFS to local file system. 
    Also, we can pass multiple files or folders in HDFS to get command.

%%sh

rm -rf /home/${USER}/retail_db

%%sh

ls -ltr /home/${USER}

%%sh

mkdir /home/${USER}/retail_db

%%sh

hdfs dfs -get /user/${USER}/retail_db/order* /home/${USER}/retail_db

%%sh

ls -ltr /home/${USER}/retail_db

%%sh

hdfs dfs -get /user/${USER}/retail_db/departments /user/${USER}/retail_db/products /home/${USER}/retail_db

%%sh

ls -ltr /home/${USER}/retail_db

%%sh

hdfs dfs -get /user/${USER}/retail_db/categories /user/${USER}/retail_db/customers /home/${USER}/retail_db

%%sh

ls -ltr /home/${USER}/retail_db




Getting File Metadata¶

Let us see how to get metadata for the files stored in HDFS using hdfs fsck command.

    We have files copied under HDFS location /user/${USER}/retail_db. We also have some sample large files copied under HDFS location /public/randomtextwriter. We can use hdfs fsck command.

    We will first see how to get metadata of these files and then try to interpret it in subsequent topics.

    HDFS stands for Hadoop Distributed File System. It means files are copied in distributed fashion.

    Our cluster have master nodes and worker nodes, in this case the files will be physically copied in the worker nodes where data node process is running. We will cover this as part of the HDFS architecture.

    Here are the details about worker nodes along with corresponding private ips.

Private ip
    

Full DNS
    

Short DNS

172.16.1.102
    

wn01.itversity.com
    

wn01

172.16.1.103
    

wn02.itversity.com
    

wn02

172.16.1.104
    

wn03.itversity.com
    

wn03

172.16.1.107
    

wn04.itversity.com
    

wn04

172.16.1.108
    

wn05.itversity.com
    

wn05

%%sh

hdfs fsck -help

Usage: hdfs fsck <path> [-list-corruptfileblocks | [-move | -delete | -openforwrite] [-files [-blocks [-locations | -racks]]]] [-includeSnapshots] [-storagepolicies] [-blockId <blk_Id>]
    <path>  start checking from this path
    -move   move corrupted files to /lost+found
    -delete delete corrupted files
    -files  print out files being checked
    -openforwrite   print out files opened for write
    -includeSnapshots   include snapshot data if the given path indicates a snapshottable directory or there are snapshottable directories under it
    -list-corruptfileblocks print out list of missing blocks and files they belong to
    -blocks print out block report
    -locations  print out locations for every block
    -racks  print out network topology for data-node locations
    -storagepolicies    print out storage policy summary for the blocks
    -blockId    print out which file this blockId belongs to, locations (nodes, racks) of this block, and other diagnostics info (under replicated, corrupted or not, etc)
    -replicaDetails print out each replica details 

Please Note:
    1. By default fsck ignores files opened for write, use -openforwrite to report such files. They are usually  tagged CORRUPT or HEALTHY depending on their block allocation status
    2. Option -includeSnapshots should not be used for comparing stats, should be used only for HEALTH check, as this may contain duplicates if the same file present in both original fs tree and inside snapshots.

Generic options supported are
-conf <configuration file>     specify an application configuration file
-D <property=value>            use value for given property
-fs <local|namenode:port>      specify a namenode
-jt <local|resourcemanager:port>    specify a ResourceManager
-files <comma separated list of files>    specify comma separated files to be copied to the map reduce cluster
-libjars <comma separated list of jars>    specify comma separated jar files to include in the classpath.
-archives <comma separated list of archives>    specify comma separated archives to be unarchived on the compute machines.

The general command line syntax is
bin/hadoop command [genericOptions] [commandOptions]

    We can get high level overview for a retail_db folder by using hdfs fsck retail_db

%%sh

hdfs fsck /user/${USER}/retail_db

FSCK started by itversity (auth:SIMPLE) from /172.16.1.114 for path /user/itversity/retail_db at Thu Jan 21 05:34:39 EST 2021
......Status: HEALTHY
 Total size:    9537787 B
 Total dirs:    7
 Total files:   6
 Total symlinks:        0
 Total blocks (validated):  6 (avg. block size 1589631 B)
 Minimally replicated blocks:   6 (100.0 %)
 Over-replicated blocks:    0 (0.0 %)
 Under-replicated blocks:   0 (0.0 %)
 Mis-replicated blocks:     0 (0.0 %)
 Default replication factor:    2
 Average block replication: 2.0
 Corrupt blocks:        0
 Missing replicas:      0 (0.0 %)
 Number of data-nodes:      5
 Number of racks:       1
FSCK ended at Thu Jan 21 05:34:39 EST 2021 in 1 milliseconds


The filesystem under path '/user/itversity/retail_db' is HEALTHY

Connecting to namenode via http://172.16.1.101:50070/fsck?ugi=itversity&path=%2Fuser%2Fitversity%2Fretail_db

    We can get details about file names using -files option.

%%sh

hdfs fsck /user/${USER}/retail_db -files

FSCK started by itversity (auth:SIMPLE) from /172.16.1.114 for path /user/itversity/retail_db at Thu Jan 21 05:35:17 EST 2021
/user/itversity/retail_db <dir>
/user/itversity/retail_db/categories <dir>
/user/itversity/retail_db/categories/part-00000 1029 bytes, 1 block(s):  OK
/user/itversity/retail_db/customers <dir>
/user/itversity/retail_db/customers/part-00000 953719 bytes, 1 block(s):  OK
/user/itversity/retail_db/departments <dir>
/user/itversity/retail_db/departments/part-00000 60 bytes, 1 block(s):  OK
/user/itversity/retail_db/order_items <dir>
/user/itversity/retail_db/order_items/part-00000 5408880 bytes, 1 block(s):  OK
/user/itversity/retail_db/orders <dir>
/user/itversity/retail_db/orders/part-00000 2999944 bytes, 1 block(s):  OK
/user/itversity/retail_db/products <dir>
/user/itversity/retail_db/products/part-00000 174155 bytes, 1 block(s):  OK
Status: HEALTHY
 Total size:    9537787 B
 Total dirs:    7
 Total files:   6
 Total symlinks:        0
 Total blocks (validated):  6 (avg. block size 1589631 B)
 Minimally replicated blocks:   6 (100.0 %)
 Over-replicated blocks:    0 (0.0 %)
 Under-replicated blocks:   0 (0.0 %)
 Mis-replicated blocks:     0 (0.0 %)
 Default replication factor:    2
 Average block replication: 2.0
 Corrupt blocks:        0
 Missing replicas:      0 (0.0 %)
 Number of data-nodes:      5
 Number of racks:       1
FSCK ended at Thu Jan 21 05:35:17 EST 2021 in 1 milliseconds


The filesystem under path '/user/itversity/retail_db' is HEALTHY

Connecting to namenode via http://172.16.1.101:50070/fsck?ugi=itversity&files=1&path=%2Fuser%2Fitversity%2Fretail_db

    Files in HDFS will be physically stored in worker nodes as blocks. We can get details of blocks associated with files using -blocks option.

%%sh

hdfs fsck /user/${USER}/retail_db -files -blocks

FSCK started by itversity (auth:SIMPLE) from /172.16.1.114 for path /user/itversity/retail_db at Thu Jan 21 05:36:09 EST 2021
/user/itversity/retail_db <dir>
/user/itversity/retail_db/categories <dir>
/user/itversity/retail_db/categories/part-00000 1029 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455898_41737435 len=1029 repl=2

/user/itversity/retail_db/customers <dir>
/user/itversity/retail_db/customers/part-00000 953719 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455899_41737436 len=953719 repl=2

/user/itversity/retail_db/departments <dir>
/user/itversity/retail_db/departments/part-00000 60 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455900_41737437 len=60 repl=2

/user/itversity/retail_db/order_items <dir>
/user/itversity/retail_db/order_items/part-00000 5408880 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455901_41737438 len=5408880 repl=2

/user/itversity/retail_db/orders <dir>
/user/itversity/retail_db/orders/part-00000 2999944 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455902_41737439 len=2999944 repl=2

/user/itversity/retail_db/products <dir>
/user/itversity/retail_db/products/part-00000 174155 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455903_41737440 len=174155 repl=2

Status: HEALTHY
 Total size:    9537787 B
 Total dirs:    7
 Total files:   6
 Total symlinks:        0
 Total blocks (validated):  6 (avg. block size 1589631 B)
 Minimally replicated blocks:   6 (100.0 %)
 Over-replicated blocks:    0 (0.0 %)
 Under-replicated blocks:   0 (0.0 %)
 Mis-replicated blocks:     0 (0.0 %)
 Default replication factor:    2
 Average block replication: 2.0
 Corrupt blocks:        0
 Missing replicas:      0 (0.0 %)
 Number of data-nodes:      5
 Number of racks:       1
FSCK ended at Thu Jan 21 05:36:09 EST 2021 in 1 milliseconds


The filesystem under path '/user/itversity/retail_db' is HEALTHY

Connecting to namenode via http://172.16.1.101:50070/fsck?ugi=itversity&files=1&blocks=1&path=%2Fuser%2Fitversity%2Fretail_db

    -blocks will only provide details about the names of the blocks, we need to use -locations as well to get the details about the worker nodes where the blocks are physically stored.

    A block is nothing but a physical file in HDFS. We will understand more about blocks as part of the subsequent topics.

    To understand where a block is physically stored you can get the infromation from DatanodeInfoWithStorage part of the output. It will contain ip address and we can get the corresponding DNS from the above table.

%%sh

hdfs fsck /user/${USER}/retail_db -files -blocks -locations

FSCK started by itversity (auth:SIMPLE) from /172.16.1.114 for path /user/itversity/retail_db at Thu Jan 21 05:38:08 EST 2021
/user/itversity/retail_db <dir>
/user/itversity/retail_db/categories <dir>
/user/itversity/retail_db/categories/part-00000 1029 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455898_41737435 len=1029 repl=2 [DatanodeInfoWithStorage[172.16.1.108:50010,DS-698dde50-a336-4e00-bc8f-a9e1a5cc76f4,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-7fb58858-abe9-4a52-9b75-755d849a897b,DISK]]

/user/itversity/retail_db/customers <dir>
/user/itversity/retail_db/customers/part-00000 953719 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455899_41737436 len=953719 repl=2 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.104:50010,DS-98fec5a6-72a9-4590-99cc-cee3a51f4dd5,DISK]]

/user/itversity/retail_db/departments <dir>
/user/itversity/retail_db/departments/part-00000 60 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455900_41737437 len=60 repl=2 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-a12c4ae3-3f6a-42fc-83ff-7779a9fc0482,DISK]]

/user/itversity/retail_db/order_items <dir>
/user/itversity/retail_db/order_items/part-00000 5408880 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455901_41737438 len=5408880 repl=2 [DatanodeInfoWithStorage[172.16.1.107:50010,DS-6679d10e-378c-4897-8c0e-250aa1af790a,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-1f4edfab-2926-45f9-a37c-ae9d1f542680,DISK]]

/user/itversity/retail_db/orders <dir>
/user/itversity/retail_db/orders/part-00000 2999944 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455902_41737439 len=2999944 repl=2 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-736614f7-27de-46b8-987f-d669be6a32a3,DISK]]

/user/itversity/retail_db/products <dir>
/user/itversity/retail_db/products/part-00000 174155 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455903_41737440 len=174155 repl=2 [DatanodeInfoWithStorage[172.16.1.108:50010,DS-698dde50-a336-4e00-bc8f-a9e1a5cc76f4,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-7fb58858-abe9-4a52-9b75-755d849a897b,DISK]]

Status: HEALTHY
 Total size:    9537787 B
 Total dirs:    7
 Total files:   6
 Total symlinks:        0
 Total blocks (validated):  6 (avg. block size 1589631 B)
 Minimally replicated blocks:   6 (100.0 %)
 Over-replicated blocks:    0 (0.0 %)
 Under-replicated blocks:   0 (0.0 %)
 Mis-replicated blocks:     0 (0.0 %)
 Default replication factor:    2
 Average block replication: 2.0
 Corrupt blocks:        0
 Missing replicas:      0 (0.0 %)
 Number of data-nodes:      5
 Number of racks:       1
FSCK ended at Thu Jan 21 05:38:08 EST 2021 in 1 milliseconds


The filesystem under path '/user/itversity/retail_db' is HEALTHY

Connecting to namenode via http://172.16.1.101:50070/fsck?ugi=itversity&files=1&blocks=1&locations=1&path=%2Fuser%2Fitversity%2Fretail_db

%%sh

hdfs dfs -ls -h /public/randomtextwriter/part-m-00000

-rw-r--r--   3 hdfs hdfs      1.0 G 2017-01-18 20:24 /public/randomtextwriter/part-m-00000

%%sh

hdfs fsck /public/randomtextwriter/part-m-00000 -files -blocks -locations

FSCK started by itversity (auth:SIMPLE) from /172.16.1.114 for path /public/randomtextwriter/part-m-00000 at Thu Jan 21 05:39:53 EST 2021
/public/randomtextwriter/part-m-00000 1102230331 bytes, 9 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1074171511_431441 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-1f4edfab-2926-45f9-a37c-ae9d1f542680,DISK]]
1. BP-292116404-172.16.1.101-1479167821718:blk_1074171524_431454 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-1f4edfab-2926-45f9-a37c-ae9d1f542680,DISK]]
2. BP-292116404-172.16.1.101-1479167821718:blk_1074171559_431489 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-1f4edfab-2926-45f9-a37c-ae9d1f542680,DISK]]
3. BP-292116404-172.16.1.101-1479167821718:blk_1074171609_431539 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-7fb58858-abe9-4a52-9b75-755d849a897b,DISK]]
4. BP-292116404-172.16.1.101-1479167821718:blk_1074171657_431587 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-a12c4ae3-3f6a-42fc-83ff-7779a9fc0482,DISK]]
5. BP-292116404-172.16.1.101-1479167821718:blk_1074171691_431621 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-7fb58858-abe9-4a52-9b75-755d849a897b,DISK]]
6. BP-292116404-172.16.1.101-1479167821718:blk_1074171721_431651 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-6679d10e-378c-4897-8c0e-250aa1af790a,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-736614f7-27de-46b8-987f-d669be6a32a3,DISK]]
7. BP-292116404-172.16.1.101-1479167821718:blk_1074171731_431661 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-a12c4ae3-3f6a-42fc-83ff-7779a9fc0482,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-698dde50-a336-4e00-bc8f-a9e1a5cc76f4,DISK]]
8. BP-292116404-172.16.1.101-1479167821718:blk_1074171736_431666 len=28488507 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-6679d10e-378c-4897-8c0e-250aa1af790a,DISK]]

Status: HEALTHY
 Total size:    1102230331 B
 Total dirs:    0
 Total files:   1
 Total symlinks:        0
 Total blocks (validated):  9 (avg. block size 122470036 B)
 Minimally replicated blocks:   9 (100.0 %)
 Over-replicated blocks:    0 (0.0 %)
 Under-replicated blocks:   0 (0.0 %)
 Mis-replicated blocks:     0 (0.0 %)
 Default replication factor:    2
 Average block replication: 3.0
 Corrupt blocks:        0
 Missing replicas:      0 (0.0 %)
 Number of data-nodes:      5
 Number of racks:       1
FSCK ended at Thu Jan 21 05:39:53 EST 2021 in 0 milliseconds


The filesystem under path '/public/randomtextwriter/part-m-00000' is HEALTHY

Connecting to namenode via http://172.16.1.101:50070/fsck?ugi=itversity&files=1&blocks=1&locations=1&path=%2Fpublic%2Frandomtextwriter%2Fpart-m-00000


Previewing data in HDFS Files¶

Let us see how we can preview the data in HDFS.

    If we are dealing with files contain text data (files of text file format), we can preview contents of the files using different commands as -tail, -cat etc.

    -tail can be used to preview last 1 KB of the file

    -cat can be used to print the whole contents of the file on the screen. Be careful while using -cat as it will take a while for even medium sized files.

    If you want to get first few lines from file you can redirect output of hadoop fs -cat or hdfs dfs -cat to Linux more command

%%sh

hdfs dfs -ls /user/${USER}/retail_db

Found 6 items
drwxr-xr-x   - itversity students          0 2021-01-17 20:05 /user/itversity/retail_db/categories
drwxr-xr-x   - itversity students          0 2021-01-17 20:05 /user/itversity/retail_db/customers
drwxr-xr-x   - itversity students          0 2021-01-17 20:04 /user/itversity/retail_db/departments
drwxr-xr-x   - itversity students          0 2021-01-17 20:03 /user/itversity/retail_db/order_items
drwxr-xr-x   - itversity students          0 2021-01-17 20:03 /user/itversity/retail_db/orders
drwxr-xr-x   - itversity students          0 2021-01-17 20:04 /user/itversity/retail_db/products

%%sh

hdfs dfs -ls -R /user/${USER}/retail_db

drwxr-xr-x   - itversity students          0 2021-01-17 20:05 /user/itversity/retail_db/categories
-rw-r--r--   2 itversity students       1029 2021-01-17 20:05 /user/itversity/retail_db/categories/part-00000
drwxr-xr-x   - itversity students          0 2021-01-17 20:05 /user/itversity/retail_db/customers
-rw-r--r--   2 itversity students     953719 2021-01-17 20:05 /user/itversity/retail_db/customers/part-00000
drwxr-xr-x   - itversity students          0 2021-01-17 20:04 /user/itversity/retail_db/departments
-rw-r--r--   2 itversity students         60 2021-01-17 20:04 /user/itversity/retail_db/departments/part-00000
drwxr-xr-x   - itversity students          0 2021-01-17 20:03 /user/itversity/retail_db/order_items
-rw-r--r--   2 itversity students    5408880 2021-01-17 20:03 /user/itversity/retail_db/order_items/part-00000
drwxr-xr-x   - itversity students          0 2021-01-17 20:03 /user/itversity/retail_db/orders
-rw-r--r--   2 itversity students    2999944 2021-01-17 20:03 /user/itversity/retail_db/orders/part-00000
drwxr-xr-x   - itversity students          0 2021-01-17 20:04 /user/itversity/retail_db/products
-rw-r--r--   2 itversity students     174155 2021-01-17 20:04 /user/itversity/retail_db/products/part-00000

%%sh

hdfs dfs -put -f /data/retail_db /user/${USER}/

%%sh

hdfs dfs -help tail

-tail [-f] <file> :
  Show the last 1KB of the file.
                                             
  -f  Shows appended data as the file grows. 

%%sh

hdfs dfs -tail /user/${USER}/retail_db/orders/part-00000

014-06-12 00:00:00.0,4229,PENDING
68861,2014-06-13 00:00:00.0,3031,PENDING_PAYMENT
68862,2014-06-15 00:00:00.0,7326,PROCESSING
68863,2014-06-16 00:00:00.0,3361,CLOSED
68864,2014-06-18 00:00:00.0,9634,ON_HOLD
68865,2014-06-19 00:00:00.0,4567,SUSPECTED_FRAUD
68866,2014-06-20 00:00:00.0,3890,PENDING_PAYMENT
68867,2014-06-23 00:00:00.0,869,CANCELED
68868,2014-06-24 00:00:00.0,10184,PENDING
68869,2014-06-25 00:00:00.0,7456,PROCESSING
68870,2014-06-26 00:00:00.0,3343,COMPLETE
68871,2014-06-28 00:00:00.0,4960,PENDING
68872,2014-06-29 00:00:00.0,3354,COMPLETE
68873,2014-06-30 00:00:00.0,4545,PENDING
68874,2014-07-03 00:00:00.0,1601,COMPLETE
68875,2014-07-04 00:00:00.0,10637,ON_HOLD
68876,2014-07-06 00:00:00.0,4124,COMPLETE
68877,2014-07-07 00:00:00.0,9692,ON_HOLD
68878,2014-07-08 00:00:00.0,6753,COMPLETE
68879,2014-07-09 00:00:00.0,778,COMPLETE
68880,2014-07-13 00:00:00.0,1117,COMPLETE
68881,2014-07-19 00:00:00.0,2518,PENDING_PAYMENT
68882,2014-07-22 00:00:00.0,10000,ON_HOLD
68883,2014-07-23 00:00:00.0,5533,COMPLETE

%%sh

hdfs dfs -help cat

-cat [-ignoreCrc] <src> ... :
  Fetch all files that match the file pattern <src> and display their content on
  stdout.

%%sh

hdfs dfs -cat /user/${USER}/retail_db/departments/part-*

2,Fitness
3,Footwear
4,Apparel
5,Golf
6,Outdoors
7,Fan Shop

    You can run the following command using terminal or CLI to see first few lines in a file.

hdfs dfs -cat /user/${USER}/retail_db/orders/part-00000|more



HDFS Blocksize¶

Let us get into details related to blocksize in HDFS.

    HDFS stands for Hadoop Distributed File System.

    It means the large files will be physically stored on multiple nodes in distributed fashion.

    Let us review the hdfs fsck output of /public/randomtextwriter/part-m-00000. The file is approximately 1 GB in size and you will see 9 files.

        8 files of size 128 MB

        1 file of size 28 MB approximately

    It means a file of size 1 GB 28 MB is stored in 9 blocks. It is due to the default block size which is 128 MB.

%%sh

hdfs dfs -ls -h /public/randomtextwriter/part-m-00000

-rw-r--r--   3 hdfs hdfs      1.0 G 2017-01-18 20:24 /public/randomtextwriter/part-m-00000

%%sh

hdfs fsck /public/randomtextwriter/part-m-00000 \
    -files \
    -blocks \
    -locations

FSCK started by itversity (auth:SIMPLE) from /172.16.1.114 for path /public/randomtextwriter/part-m-00000 at Thu Jan 21 05:42:10 EST 2021
/public/randomtextwriter/part-m-00000 1102230331 bytes, 9 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1074171511_431441 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-1f4edfab-2926-45f9-a37c-ae9d1f542680,DISK]]
1. BP-292116404-172.16.1.101-1479167821718:blk_1074171524_431454 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-1f4edfab-2926-45f9-a37c-ae9d1f542680,DISK]]
2. BP-292116404-172.16.1.101-1479167821718:blk_1074171559_431489 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-1f4edfab-2926-45f9-a37c-ae9d1f542680,DISK]]
3. BP-292116404-172.16.1.101-1479167821718:blk_1074171609_431539 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-7fb58858-abe9-4a52-9b75-755d849a897b,DISK]]
4. BP-292116404-172.16.1.101-1479167821718:blk_1074171657_431587 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-a12c4ae3-3f6a-42fc-83ff-7779a9fc0482,DISK]]
5. BP-292116404-172.16.1.101-1479167821718:blk_1074171691_431621 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-7fb58858-abe9-4a52-9b75-755d849a897b,DISK]]
6. BP-292116404-172.16.1.101-1479167821718:blk_1074171721_431651 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-6679d10e-378c-4897-8c0e-250aa1af790a,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-736614f7-27de-46b8-987f-d669be6a32a3,DISK]]
7. BP-292116404-172.16.1.101-1479167821718:blk_1074171731_431661 len=134217728 repl=3 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-a12c4ae3-3f6a-42fc-83ff-7779a9fc0482,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-698dde50-a336-4e00-bc8f-a9e1a5cc76f4,DISK]]
8. BP-292116404-172.16.1.101-1479167821718:blk_1074171736_431666 len=28488507 repl=3 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-6679d10e-378c-4897-8c0e-250aa1af790a,DISK]]

Status: HEALTHY
 Total size:    1102230331 B
 Total dirs:    0
 Total files:   1
 Total symlinks:        0
 Total blocks (validated):  9 (avg. block size 122470036 B)
 Minimally replicated blocks:   9 (100.0 %)
 Over-replicated blocks:    0 (0.0 %)
 Under-replicated blocks:   0 (0.0 %)
 Mis-replicated blocks:     0 (0.0 %)
 Default replication factor:    2
 Average block replication: 3.0
 Corrupt blocks:        0
 Missing replicas:      0 (0.0 %)
 Number of data-nodes:      5
 Number of racks:       1
FSCK ended at Thu Jan 21 05:42:10 EST 2021 in 1 milliseconds


The filesystem under path '/public/randomtextwriter/part-m-00000' is HEALTHY

Connecting to namenode via http://172.16.1.101:50070/fsck?ugi=itversity&files=1&blocks=1&locations=1&path=%2Fpublic%2Frandomtextwriter%2Fpart-m-00000

    The default block size is 128 MB and it is set as part of hdfs-site.xml.

    The property name is dfs.blocksize.

    If the file size is smaller than default blocksize (128 MB), then there will be only one block as per the size of the file.

%%sh

cat /etc/hadoop/conf/hdfs-site.xml

    Let us determine the number of blocks for /data/retail_db/orders/part-00000. If we store this file of size 2.9 MB in HDFS, there will be one block associated with it as size of the file is less than the block size.

    It occupies 2.9 MB storage in HDFS (assuming replication factor as 1)

%%sh

ls -lhtr /data/retail_db/orders/part-00000

-rw-r--r-- 1 root root 2.9M Feb 20  2017 /data/retail_db/orders/part-00000

%%sh

hdfs fsck /user/${USER}/retail_db/orders/part-00000 -files -blocks -locations

FSCK started by itversity (auth:SIMPLE) from /172.16.1.114 for path /user/itversity/retail_db/orders/part-00000 at Thu Jan 21 05:43:52 EST 2021
/user/itversity/retail_db/orders/part-00000 2999944 bytes, 1 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1115455902_41737439 len=2999944 repl=2 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-736614f7-27de-46b8-987f-d669be6a32a3,DISK]]

Status: HEALTHY
 Total size:    2999944 B
 Total dirs:    0
 Total files:   1
 Total symlinks:        0
 Total blocks (validated):  1 (avg. block size 2999944 B)
 Minimally replicated blocks:   1 (100.0 %)
 Over-replicated blocks:    0 (0.0 %)
 Under-replicated blocks:   0 (0.0 %)
 Mis-replicated blocks:     0 (0.0 %)
 Default replication factor:    2
 Average block replication: 2.0
 Corrupt blocks:        0
 Missing replicas:      0 (0.0 %)
 Number of data-nodes:      5
 Number of racks:       1
FSCK ended at Thu Jan 21 05:43:52 EST 2021 in 1 milliseconds


The filesystem under path '/user/itversity/retail_db/orders/part-00000' is HEALTHY

Connecting to namenode via http://172.16.1.101:50070/fsck?ugi=itversity&files=1&blocks=1&locations=1&path=%2Fuser%2Fitversity%2Fretail_db%2Forders%2Fpart-00000

    Let us determine the number of blocks for /data/yelp-dataset-json/yelp_academic_dataset_user.json. If we store this file of size 2.4 GB in HDFS, there will be 19 blocks associated with it

        18 128 MB Files

        1 ~69 MB File

    It occupies 2.4 GB storage in HDFS (assuming replication factor as 1)

%%sh

ls -lhtr /data/yelp-dataset-json/yelp_academic_dataset_user.json

-rwxr-xr-x 1 training training 2.4G Feb  5  2019 /data/yelp-dataset-json/yelp_academic_dataset_user.json

    We can validate by using hdfs fsck command against the same file in HDFS.

%%sh

hdfs fsck /public/yelp-dataset-json/yelp_academic_dataset_user.json \
    -files \
    -blocks \
    -locations

FSCK started by itversity (auth:SIMPLE) from /172.16.1.114 for path /public/yelp-dataset-json/yelp_academic_dataset_user.json at Thu Jan 21 05:44:47 EST 2021
/public/yelp-dataset-json/yelp_academic_dataset_user.json 2485747393 bytes, 19 block(s):  OK
0. BP-292116404-172.16.1.101-1479167821718:blk_1101225469_27499779 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.107:50010,DS-a12c4ae3-3f6a-42fc-83ff-7779a9fc0482,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-698dde50-a336-4e00-bc8f-a9e1a5cc76f4,DISK]]
1. BP-292116404-172.16.1.101-1479167821718:blk_1101225470_27499780 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.103:50010,DS-7fb58858-abe9-4a52-9b75-755d849a897b,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-736614f7-27de-46b8-987f-d669be6a32a3,DISK]]
2. BP-292116404-172.16.1.101-1479167821718:blk_1101225471_27499781 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-698dde50-a336-4e00-bc8f-a9e1a5cc76f4,DISK]]
3. BP-292116404-172.16.1.101-1479167821718:blk_1101225472_27499782 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-6679d10e-378c-4897-8c0e-250aa1af790a,DISK]]
4. BP-292116404-172.16.1.101-1479167821718:blk_1101225473_27499783 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-98fec5a6-72a9-4590-99cc-cee3a51f4dd5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK]]
5. BP-292116404-172.16.1.101-1479167821718:blk_1101225474_27499784 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK]]
6. BP-292116404-172.16.1.101-1479167821718:blk_1101225475_27499785 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-98fec5a6-72a9-4590-99cc-cee3a51f4dd5,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-1f4edfab-2926-45f9-a37c-ae9d1f542680,DISK]]
7. BP-292116404-172.16.1.101-1479167821718:blk_1101225476_27499786 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.103:50010,DS-7fb58858-abe9-4a52-9b75-755d849a897b,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-736614f7-27de-46b8-987f-d669be6a32a3,DISK]]
8. BP-292116404-172.16.1.101-1479167821718:blk_1101225477_27499787 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-698dde50-a336-4e00-bc8f-a9e1a5cc76f4,DISK]]
9. BP-292116404-172.16.1.101-1479167821718:blk_1101225478_27499788 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK]]
10. BP-292116404-172.16.1.101-1479167821718:blk_1101225479_27499789 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-98fec5a6-72a9-4590-99cc-cee3a51f4dd5,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-1f4edfab-2926-45f9-a37c-ae9d1f542680,DISK]]
11. BP-292116404-172.16.1.101-1479167821718:blk_1101225480_27499790 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.107:50010,DS-a12c4ae3-3f6a-42fc-83ff-7779a9fc0482,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-7fb58858-abe9-4a52-9b75-755d849a897b,DISK]]
12. BP-292116404-172.16.1.101-1479167821718:blk_1101225481_27499791 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.107:50010,DS-6679d10e-378c-4897-8c0e-250aa1af790a,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-736614f7-27de-46b8-987f-d669be6a32a3,DISK]]
13. BP-292116404-172.16.1.101-1479167821718:blk_1101225482_27499792 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.107:50010,DS-a12c4ae3-3f6a-42fc-83ff-7779a9fc0482,DISK], DatanodeInfoWithStorage[172.16.1.103:50010,DS-1f4edfab-2926-45f9-a37c-ae9d1f542680,DISK]]
14. BP-292116404-172.16.1.101-1479167821718:blk_1101225483_27499793 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-1edb1d35-81bf-471b-be04-11d973e2a832,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-698dde50-a336-4e00-bc8f-a9e1a5cc76f4,DISK]]
15. BP-292116404-172.16.1.101-1479167821718:blk_1101225484_27499794 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.102:50010,DS-b0f1636e-fd08-4ddb-bba9-9df8868dfb5d,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-736614f7-27de-46b8-987f-d669be6a32a3,DISK]]
16. BP-292116404-172.16.1.101-1479167821718:blk_1101225485_27499795 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.107:50010,DS-6679d10e-378c-4897-8c0e-250aa1af790a,DISK], DatanodeInfoWithStorage[172.16.1.108:50010,DS-698dde50-a336-4e00-bc8f-a9e1a5cc76f4,DISK]]
17. BP-292116404-172.16.1.101-1479167821718:blk_1101225486_27499796 len=134217728 repl=2 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-f4667aac-0f2c-463c-9584-d625928b9af5,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-a12c4ae3-3f6a-42fc-83ff-7779a9fc0482,DISK]]
18. BP-292116404-172.16.1.101-1479167821718:blk_1101225487_27499797 len=69828289 repl=2 [DatanodeInfoWithStorage[172.16.1.104:50010,DS-98fec5a6-72a9-4590-99cc-cee3a51f4dd5,DISK], DatanodeInfoWithStorage[172.16.1.107:50010,DS-6679d10e-378c-4897-8c0e-250aa1af790a,DISK]]

Status: HEALTHY
 Total size:    2485747393 B
 Total dirs:    0
 Total files:   1
 Total symlinks:        0
 Total blocks (validated):  19 (avg. block size 130828810 B)
 Minimally replicated blocks:   19 (100.0 %)
 Over-replicated blocks:    0 (0.0 %)
 Under-replicated blocks:   0 (0.0 %)
 Mis-replicated blocks:     0 (0.0 %)
 Default replication factor:    2
 Average block replication: 2.0
 Corrupt blocks:        0
 Missing replicas:      0 (0.0 %)
 Number of data-nodes:      5
 Number of racks:       1
FSCK ended at Thu Jan 21 05:44:47 EST 2021 in 1 milliseconds


The filesystem under path '/public/yelp-dataset-json/yelp_academic_dataset_user.json' is HEALTHY

Connecting to namenode via http://172.16.1.101:50070/fsck?ugi=itversity&files=1&blocks=1&locations=1&path=%2Fpublic%2Fyelp-dataset-json%2Fyelp_academic_dataset_user.json





